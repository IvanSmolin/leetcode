package ivan.smolin.core;

public final class ArrayUtils {
    public static void print(int[][] array) {
        for (int[] col : array) {
            for (int row : col) {
                System.out.print(row + " ");
            }

            System.out.println();
        }
    }
}