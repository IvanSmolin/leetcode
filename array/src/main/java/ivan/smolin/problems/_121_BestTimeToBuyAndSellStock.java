package ivan.smolin.problems;

/**
 * LeetCode Problem 121. Best Time to Buy and Sell Stock:
 * https://leetcode.com/problems/best-time-to-buy-and-sell-stock/
 */
public final class _121_BestTimeToBuyAndSellStock {
    public static int maxProfit(int[] prices) {
        var min = prices[0];
        var max = 0;
        var profit = 0;

        for (var i = 1; i < prices.length; i++) {
            if (prices[i] < min) {
                min = prices[i];
            }

            profit = prices[i] - min;

            if (profit > max) {
                max = profit;
            }
        }

        return max;
    }
}