package ivan.smolin.problems;

import java.util.HashMap;

/**
 * LeetCode Problem 136. Single Number: https://leetcode.com/problems/single-number/
 */
public final class _136_SingleNumber {
    public static int singleNumber(int[] nums) {
        var map = new HashMap<Integer, Integer>();

        for (var num : nums) {
            map.put(num, map.getOrDefault(num, 0) + 1);
        }

        for (var entry : map.entrySet()) {
            if (entry.getValue() == 1) {
                return entry.getKey();
            }
        }

        return 0;
    }
}