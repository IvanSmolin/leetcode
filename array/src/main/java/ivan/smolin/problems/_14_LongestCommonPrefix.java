package ivan.smolin.problems;

/**
 * LeetCode Problem 14. Longest Common Prefix: https://leetcode.com/problems/longest-common-prefix/
 */
public final class _14_LongestCommonPrefix {
    public static String longestCommonPrefix(String[] strs) {
        var answer = strs[0];

        for (int i = 1; i < strs.length; i++) {
            while (!answer.equals("")) {
                if (strs[i].startsWith(answer)) {
                    break;
                }

                answer = answer.substring(0, answer.length() - 1);
            }
        }

        return answer;
    }
}