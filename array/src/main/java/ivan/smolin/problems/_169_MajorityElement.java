package ivan.smolin.problems;

import java.util.HashMap;

/**
 * LeetCode Problem 169. Majority Element: https://leetcode.com/problems/majority-element/
 */
public final class _169_MajorityElement {
    public static int majorityElement(int[] nums) {
        var map = new HashMap<Integer, Integer>();
        var result = 0;
        var max = 0;

        for (var num : nums) {
            map.put(num, map.getOrDefault(num, 0) + 1);
        }

        for (var entry : map.entrySet()) {
            if (max < entry.getValue()) {
                result = entry.getKey();
                max = entry.getValue();
            }
        }

        return result;
    }
}