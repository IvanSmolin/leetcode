package ivan.smolin.problems;

import java.util.HashMap;
import java.util.Map;

/**
 * LeetCode Problem 1. Two Sum: https://leetcode.com/problems/two-sum
 */
public final class _1_TwoSum {
    public static int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();

        for (int i = 0; i < nums.length; i++) {
            Integer requiredNum = target - nums[i];

            if (map.containsKey(requiredNum)) {
                return new int[]{map.get(requiredNum), i};
            }

            map.put(nums[i], i);
        }

        return new int[]{};
    }
}