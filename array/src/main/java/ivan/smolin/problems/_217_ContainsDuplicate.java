package ivan.smolin.problems;

import java.util.HashSet;

/**
 * LeetCode Problem 217. Contains Duplicate: https://leetcode.com/problems/contains-duplicate/
 */
public final class _217_ContainsDuplicate {
    public static boolean containsDuplicate(int[] nums) {
        var set = new HashSet<>(nums.length, 1);

        for (int num : nums) {
            if (!set.add(num)) {
                return true;
            }
        }

        return false;
    }
}