package ivan.smolin.problems;

import java.util.HashMap;

/**
 * LeetCode Problem 219. Contains Duplicate II: https://leetcode.com/problems/contains-duplicate-ii/
 */
public final class _219_ContainsDuplicate2 {
    public static boolean containsNearbyDuplicate(int[] nums, int k) {
        var map = new HashMap<Integer, Integer>();

        for (int i = 0; i < nums.length; i++) {
            var buf = nums[i];

            if (map.containsKey(buf)) {
                if (i - map.get(buf) <= k) {
                    return true;
                }
            }

            map.put(buf, i);
        }

        return false;
    }
}