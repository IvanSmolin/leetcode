package ivan.smolin.problems;

/**
 * LeetCode Problem 268. Missing Number: https://leetcode.com/problems/missing-number/
 */
public final class _268_MissingNumber {
    public static int missingNumber(int[] nums) {
        var result = (nums.length * (nums.length + 1)) / 2;

        for (var i : nums) {
            result -= i;
        }

        return result;
    }
}
