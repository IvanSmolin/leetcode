package ivan.smolin.problems;

/**
 * LeetCode Problem 26. Remove Duplicates from Sorted Array:
 * https://leetcode.com/problems/remove-duplicates-from-sorted-array/
 */
public final class _26_RemoveDuplicatesFromSortedArray {
    public static int removeDuplicates(int[] nums) {
        var result = 1;

        for (int i = 1; i < nums.length; i++) {
            if (nums[i] != nums[result - 1]) {
                nums[result++] = nums[i];
            }
        }

        return result;
    }
}