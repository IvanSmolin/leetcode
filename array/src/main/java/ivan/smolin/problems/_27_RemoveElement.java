package ivan.smolin.problems;

/**
 * LeetCode Problem 27. Remove Element: https://leetcode.com/problems/remove-element/
 */
public final class _27_RemoveElement {
    public static int removeElement(int[] nums, int val) {
        var count = 0;

        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != val) {
                nums[count++] = nums[i];
            }
        }

        return count;
    }
}