package ivan.smolin.problems;

/**
 * LeetCode Problem 344. Reverse String: https://leetcode.com/problems/reverse-string/
 */
public final class _344_ReverseString {
    public static char[] reverseString(char[] s) {
        for (var i = 0; i < s.length / 2; i++) {
            var buf = s[i];
            s[i] = s[s.length - i - 1];
            s[s.length - i - 1] = buf;
        }

        return s;
    }
}