package ivan.smolin.problems;

/**
 * LeetCode Problem 414. Third Maximum Number: https://leetcode.com/problems/third-maximum-number/
 */
public final class _414_ThirdMaximumNumber {
    public static int thirdMax(int[] nums) {
        int first = nums[0];
        long second = Long.MIN_VALUE;
        long third = Long.MIN_VALUE;

        for (int i = 1; i < nums.length; i++) {
            if (nums[i] > first) {
                third = second;
                second = first;
                first = nums[i];
            } else if (nums[i] > second && nums[i] < first) {
                third = second;
                second = nums[i];
            } else if (nums[i] > third && nums[i] < second) {
                third = nums[i];
            }
        }

        return third == Long.MIN_VALUE ? first : (int) third;
    }
}