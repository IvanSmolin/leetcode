package ivan.smolin.problems;

/**
 * LeetCode Problem 4. Median of Two Sorted Arrays: https://leetcode.com/problems/median-of-two-sorted-arrays/
 */
public final class _4_MedianOfTwoSortedArrays {
    public static double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int index1 = 0;
        int index2 = 0;
        int length = nums1.length + nums2.length;
        int medianMod = length % 2;
        int median = medianMod == 0 ? length / 2 : length / 2 + 1;
        int position = 0;
        boolean flag = true;

        if (nums1.length == 0) {
            if (medianMod == 0 && nums2.length > 1) {
                return ((double) nums2[median - 1] + nums2[median]) / 2;
            } else {
                return nums2[median - 1];
            }
        } else if (nums2.length == 0) {
            if (medianMod == 0 && nums1.length > 1) {
                return ((double) nums1[median - 1] + nums1[median]) / 2;
            } else {
                return nums1[median - 1];
            }
        }

        while (position != median) {
            if (index1 < nums1.length && index2 < nums2.length) {
                if (nums1[index1] < nums2[index2]) {
                    index1++;
                    flag = true;
                } else {
                    index2++;
                    flag = false;
                }
            } else if (index1 < nums1.length && index2 == nums2.length) {
                index1++;
                flag = true;
            } else {
                index2++;
                flag = false;
            }

            position++;
        }

        if (length % 2 == 0) {
            if (flag) {
                if (index1 == nums1.length) {
                    return ((double) nums1[index1 - 1] + nums2[index2]) / 2;
                } else {
                    if (index2 == nums2.length) {
                        return ((double) nums1[index1 - 1] + nums1[index1]) / 2;
                    } else {
                        return ((double) nums1[index1 - 1] + Math.min(nums1[index1], nums2[index2])) / 2;
                    }
                }
            } else {
                if (index2 == nums2.length) {
                    return ((double) nums2[index2 - 1] + nums1[index1]) / 2;
                } else {
                    if (index1 == nums1.length) {
                        return ((double) nums2[index2 - 1] + nums2[index2]) / 2;
                    } else {
                        return ((double) nums2[index2 - 1] + Math.min(nums2[index2], nums1[index1])) / 2;
                    }
                }
            }
        } else {
            if (flag) {
                return nums1[index1 - 1];
            } else {
                return nums2[index2 - 1];
            }
        }
    }
}