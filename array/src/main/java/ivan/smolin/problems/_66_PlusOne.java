package ivan.smolin.problems;

/**
 * LeetCode Problem 66. Plus One: https://leetcode.com/problems/plus-one/
 */
public final class _66_PlusOne {
    public static int[] plusOne(int[] digits) {
        for (var i = digits.length - 1; i >= 0; i--) {
            if (digits[i] != 9) {
                digits[i]++;
                return digits;
            }

            digits[i] = 0;
        }

        var result = new int[digits.length + 1];
        result[0] = 1;

        return result;
    }
}