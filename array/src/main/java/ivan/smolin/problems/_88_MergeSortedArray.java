package ivan.smolin.problems;

/**
 * LeetCode Problem 88. Merge Sorted Array: https://leetcode.com/problems/merge-sorted-array/
 */
public final class _88_MergeSortedArray {
    public static int[] merge(int[] nums1, int m, int[] nums2, int n) {
        var i = m - 1;
        var j = n - 1;
        var k = m + n - 1;

        while (i >= 0 && j >= 0) {
            if (nums1[i] < nums2[j]) {
                nums1[k] = nums2[j];
                j--;
            } else {
                nums1[k] = nums1[i];
                i--;
            }

            k--;
        }

        while (j >= 0) {
            nums1[k] = nums2[j];
            j--;
            k--;
        }

        return nums1;
    }
}