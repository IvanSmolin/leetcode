package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._121_BestTimeToBuyAndSellStock.maxProfit;

/**
 * LeetCode Problem 121. Best Time to Buy and Sell Stock:
 * https://leetcode.com/problems/best-time-to-buy-and-sell-stock/
 */
public final class _121_BestTimeToBuyAndSellStockTest {
    @Test
    void test1() {
        var actual = maxProfit(new int[]{7, 1, 5, 3, 6, 4});

        Assertions.assertEquals(5, actual);
    }

    @Test
    void test2() {
        var actual = maxProfit(new int[]{7, 6, 4, 3, 1});

        Assertions.assertEquals(0, actual);
    }

    @Test
    void test3() {
        var actual = maxProfit(new int[]{2, 4, 1});

        Assertions.assertEquals(2, actual);
    }
}