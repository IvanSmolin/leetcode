package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._136_SingleNumber.singleNumber;

/**
 * LeetCode Problem 136. Single Number: https://leetcode.com/problems/single-number/
 */
public final class _136_SingleNumberTest {
    @Test
    void test1() {
        var actual = singleNumber(new int[]{2, 2, 1});

        Assertions.assertEquals(1, actual);
    }

    @Test
    void test2() {
        var actual = singleNumber(new int[]{4, 1, 2, 1, 2});

        Assertions.assertEquals(4, actual);
    }

    @Test
    void test3() {
        var actual = singleNumber(new int[]{1});

        Assertions.assertEquals(1, actual);
    }
}