package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._14_LongestCommonPrefix.longestCommonPrefix;

/**
 * LeetCode Problem 14. Longest Common Prefix: https://leetcode.com/problems/longest-common-prefix/
 */
public final class _14_LongestCommonPrefixTest {
    @Test
    void test1() {
        var answer = longestCommonPrefix(new String[]{"sd", "sdf"});

        Assertions.assertEquals("sd", answer);
    }

    @Test
    void test2() {
        var answer = longestCommonPrefix(new String[]{"flower", "flow", "flight"});

        Assertions.assertEquals("fl", answer);
    }

    @Test
    void test3() {
        var answer = longestCommonPrefix(new String[]{"dog", "racecar", "car"});

        Assertions.assertEquals("", answer);
    }
}