package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._169_MajorityElement.majorityElement;

/**
 * LeetCode Problem 169. Majority Element: https://leetcode.com/problems/majority-element/
 */
public final class _169_MajorityElementTest {
    @Test
    void test1() {
        var answer = majorityElement(new int[]{3, 2, 3});

        Assertions.assertEquals(3, answer);
    }

    @Test
    void test2() {
        var answer = majorityElement(new int[]{2, 2, 1, 1, 1, 2, 2});

        Assertions.assertEquals(2, answer);
    }

    @Test
    void test3() {
        var answer = majorityElement(new int[]{2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2});

        Assertions.assertEquals(1, answer);
    }
}