package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._1_TwoSum.twoSum;

/**
 * LeetCode Problem 1. Two Sum: https://leetcode.com/problems/two-sum
 */
public final class _1_TwoSumTest {
    @Test
    void test1() {
        var answer = twoSum(new int[]{2, 7, 11, 15}, 9);

        Assertions.assertArrayEquals(new int[]{0, 1}, answer);
    }

    @Test
    void test2() {
        var answer = twoSum(new int[]{3, 2, 4}, 6);

        Assertions.assertArrayEquals(new int[]{1, 2}, answer);
    }

    @Test
    void test3() {
        var answer = twoSum(new int[]{3, 3}, 6);

        Assertions.assertArrayEquals(new int[]{0, 1}, answer);
    }
}