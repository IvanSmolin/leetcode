package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._217_ContainsDuplicate.containsDuplicate;

/**
 * LeetCode Problem 217. Contains Duplicate: https://leetcode.com/problems/contains-duplicate/
 */
public final class _217_ContainsDuplicateTest {
    @Test
    void test1() {
        var answer = containsDuplicate(new int[]{1, 2, 3, 1});

        Assertions.assertTrue(answer);
    }

    @Test
    void test2() {
        var answer = containsDuplicate(new int[]{1, 2, 3, 4});

        Assertions.assertFalse(answer);
    }

    @Test
    void test3() {
        var answer = containsDuplicate(new int[]{1, 1, 1, 3, 3, 4, 3, 2, 4, 2});

        Assertions.assertTrue(answer);
    }
}