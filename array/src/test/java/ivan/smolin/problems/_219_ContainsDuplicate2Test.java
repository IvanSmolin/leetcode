package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._219_ContainsDuplicate2.containsNearbyDuplicate;

/**
 * LeetCode Problem 219. Contains Duplicate II: https://leetcode.com/problems/contains-duplicate-ii/
 */
public final class _219_ContainsDuplicate2Test {
    @Test
    void test1() {
        var answer = containsNearbyDuplicate(new int[]{1, 2, 3, 1}, 3);

        Assertions.assertTrue(answer);
    }

    @Test
    void test2() {
        var answer = containsNearbyDuplicate(new int[]{1, 0, 1, 1}, 1);

        Assertions.assertTrue(answer);
    }

    @Test
    void test3() {
        var answer = containsNearbyDuplicate(new int[]{1, 2, 3, 1, 2, 3}, 2);

        Assertions.assertFalse(answer);
    }
}