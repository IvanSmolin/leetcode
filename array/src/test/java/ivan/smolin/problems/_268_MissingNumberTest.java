package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._268_MissingNumber.missingNumber;

/**
 * LeetCode Problem 268. Missing Number: https://leetcode.com/problems/missing-number/
 */
public final class _268_MissingNumberTest {
    @Test
    void test1() {
        Assertions.assertEquals(2, missingNumber(new int[]{3, 0, 1}));
    }

    @Test
    void test2() {
        Assertions.assertEquals(2, missingNumber(new int[]{0, 1}));
    }

    @Test
    void test3() {
        Assertions.assertEquals(1, missingNumber(new int[]{0}));
    }

    @Test
    void test4() {
        Assertions.assertEquals(0, missingNumber(new int[]{1}));
    }

    @Test
    void test5() {
        Assertions.assertEquals(8, missingNumber(new int[]{9, 6, 4, 2, 3, 5, 7, 0, 1}));
    }

    @Test
    void test6() {
        Assertions.assertEquals(0, missingNumber(new int[]{1, 2, 3}));
    }
}