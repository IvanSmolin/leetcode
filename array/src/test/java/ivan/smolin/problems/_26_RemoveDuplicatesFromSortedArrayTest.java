package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._26_RemoveDuplicatesFromSortedArray.removeDuplicates;

/**
 * LeetCode Problem 26. Remove Duplicates from Sorted Array:
 * https://leetcode.com/problems/remove-duplicates-from-sorted-array/
 */
public final class _26_RemoveDuplicatesFromSortedArrayTest {
    @Test
    void test1() {
        var result = removeDuplicates(new int[]{1, 1, 2});

        Assertions.assertEquals(2, result);
    }

    @Test
    void test2() {
        var result = removeDuplicates(new int[]{0, 0, 1, 1, 1, 2, 2, 3, 3, 4});

        Assertions.assertEquals(5, result);
    }
}