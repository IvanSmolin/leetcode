package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._27_RemoveElement.removeElement;

/**
 * LeetCode Problem 27. Remove Element: https://leetcode.com/problems/remove-element/
 */
public final class _27_RemoveElementTest {
    @Test
    void test1() {
        var result = removeElement(new int[]{3, 2, 2, 3}, 3);

        Assertions.assertEquals(2, result);
    }

    @Test
    void test2() {
        var result = removeElement(new int[]{0, 1, 2, 2, 3, 0, 4, 2}, 2);

        Assertions.assertEquals(5, result);
    }

    @Test
    void test3() {
        var result = removeElement(new int[]{2, 2, 2, 3, 4, 5, 0, 2}, 2);

        Assertions.assertEquals(4, result);
    }
}