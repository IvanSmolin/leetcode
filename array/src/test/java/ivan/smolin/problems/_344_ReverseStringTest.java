package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._344_ReverseString.reverseString;

/**
 * LeetCode Problem 344. Reverse String: https://leetcode.com/problems/reverse-string/
 */
public final class _344_ReverseStringTest {
    @Test
    void test1() {
        var answer = reverseString(new char[]{'h', 'e', 'l', 'l', 'o'});

        Assertions.assertArrayEquals(new char[]{'o', 'l', 'l', 'e', 'h'}, answer);
    }

    @Test
    void test2() {
        var answer = reverseString(new char[]{'H', 'a', 'n', 'n', 'a', 'h'});

        Assertions.assertArrayEquals(new char[]{'h', 'a', 'n', 'n', 'a', 'H'}, answer);
    }
}