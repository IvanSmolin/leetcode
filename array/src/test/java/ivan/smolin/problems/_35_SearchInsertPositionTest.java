package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._35_SearchInsertPosition.searchInsert;

/**
 * LeetCode Problem 35. Search Insert Position: https://leetcode.com/problems/search-insert-position/
 */
public final class _35_SearchInsertPositionTest {
    @Test
    void test1() {
        var answer = searchInsert(new int[]{1, 3, 5, 6}, 5);

        Assertions.assertEquals(2, answer);
    }

    @Test
    void test2() {
        var answer = searchInsert(new int[]{1, 3, 5, 6}, 2);

        Assertions.assertEquals(1, answer);
    }

    @Test
    void test3() {
        var answer = searchInsert(new int[]{1, 3, 5, 6}, 7);

        Assertions.assertEquals(4, answer);
    }

    @Test
    void test4() {
        var answer = searchInsert(new int[]{1, 3, 6}, 5);

        Assertions.assertEquals(2, answer);
    }

    @Test
    void test5() {
        var answer = searchInsert(new int[]{1, 3, 5, 6}, 0);

        Assertions.assertEquals(0, answer);
    }

    @Test
    void test6() {
        var answer = searchInsert(new int[]{1, 3, 5, 6, 7, 8, 9, 10, 11, 56}, 17);

        Assertions.assertEquals(9, answer);
    }

    @Test
    void test7() {
        var answer = searchInsert(new int[]{1, 3, 5, 6, 7, 8, 9, 10, 11, 56}, 57);

        Assertions.assertEquals(10, answer);
    }
}