package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._414_ThirdMaximumNumber.thirdMax;

/**
 * LeetCode Problem 414. Third Maximum Number: https://leetcode.com/problems/third-maximum-number/
 */
public final class _414_ThirdMaximumNumberTest {
    @Test
    void test1() {
        var answer = thirdMax(new int[]{3, 2, 1});

        Assertions.assertEquals(1, answer);
    }

    @Test
    void test2() {
        var answer = thirdMax(new int[]{1, 2});

        Assertions.assertEquals(2, answer);
    }

    @Test
    void test3() {
        var answer = thirdMax(new int[]{2, 2, 3, 1});

        Assertions.assertEquals(1, answer);
    }
}