package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._4_MedianOfTwoSortedArrays.findMedianSortedArrays;

/**
 * LeetCode Problem 4. Median of Two Sorted Arrays: https://leetcode.com/problems/median-of-two-sorted-arrays/
 */
public final class _4_MedianOfTwoSortedArraysTest {
    @Test
    void test1() {
        double answer = findMedianSortedArrays(new int[]{3}, new int[]{-2, -1});

        Assertions.assertEquals(-1, answer);
    }

    @Test
    void test2() {
        double answer = findMedianSortedArrays(new int[]{1, 3}, new int[]{2});

        Assertions.assertEquals(2, answer);
    }

    @Test
    void test3() {
        double answer = findMedianSortedArrays(new int[]{1, 2}, new int[]{-1, 3});

        Assertions.assertEquals(1.5, answer);
    }

    @Test
    void test4() {
        double answer = findMedianSortedArrays(new int[]{1, 2}, new int[]{3, 4});

        Assertions.assertEquals(2.5, answer);
    }

    @Test
    void test5() {
        double answer = findMedianSortedArrays(new int[]{2, 2, 2, 2}, new int[]{2, 2, 2});

        Assertions.assertEquals(2, answer);
    }

    @Test
    void test6() {
        double answer = findMedianSortedArrays(new int[]{}, new int[]{1});

        Assertions.assertEquals(1, answer);
    }

    @Test
    void test7() {
        double answer = findMedianSortedArrays(new int[]{1}, new int[]{});

        Assertions.assertEquals(1, answer);
    }

    @Test
    void test8() {
        double answer = findMedianSortedArrays(new int[]{}, new int[]{1, 2, 3, 4, 5});

        Assertions.assertEquals(3, answer);
    }

    @Test
    void test9() {
        double answer = findMedianSortedArrays(new int[]{1, 2, 3, 4, 5}, new int[]{});

        Assertions.assertEquals(3, answer);
    }

    @Test
    void test10() {
        double answer = findMedianSortedArrays(new int[]{1}, new int[]{2, 3});

        Assertions.assertEquals(2, answer);
    }

    @Test
    void test11() {
        double answer = findMedianSortedArrays(new int[]{1}, new int[]{2, 3, 4});

        Assertions.assertEquals(2.5, answer);
    }

    @Test
    void test12() {
        double answer = findMedianSortedArrays(new int[]{2, 3, 4}, new int[]{1});

        Assertions.assertEquals(2.5, answer);
    }
}