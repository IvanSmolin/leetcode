package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._66_PlusOne.plusOne;

/**
 * LeetCode Problem 66. Plus One: https://leetcode.com/problems/plus-one/
 */
public final class _66_PlusOneTest {
    @Test
    void test1() {
        var answer = plusOne(new int[]{1, 2, 3});

        Assertions.assertArrayEquals(new int[]{1, 2, 4}, answer);
    }

    @Test
    void test2() {
        var answer = plusOne(new int[]{4, 3, 2, 1});

        Assertions.assertArrayEquals(new int[]{4, 3, 2, 2}, answer);
    }

    @Test
    void test3() {
        var answer = plusOne(new int[]{9});

        Assertions.assertArrayEquals(new int[]{1, 0}, answer);
    }

    @Test
    void test4() {
        var answer = plusOne(new int[]{4, 3, 2, 9});

        Assertions.assertArrayEquals(new int[]{4, 3, 3, 0}, answer);
    }

    @Test
    void test5() {
        var answer = plusOne(new int[]{8, 9, 9, 9});

        Assertions.assertArrayEquals(new int[]{9, 0, 0, 0}, answer);
    }

    @Test
    void test6() {
        var answer = plusOne(new int[]{9, 9, 9, 9});

        Assertions.assertArrayEquals(new int[]{1, 0, 0, 0, 0}, answer);
    }

    @Test
    void test7() {
        var answer = plusOne(new int[]{9, 8, 9});

        Assertions.assertArrayEquals(new int[]{9, 9, 0}, answer);
    }
}