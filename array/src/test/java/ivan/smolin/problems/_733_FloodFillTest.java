package ivan.smolin.problems;

import ivan.smolin.core.ArrayUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._733_FloodFill.floodFill;

/**
 * LeetCode Problem 733. Flood Fill: https://leetcode.com/problems/flood-fill/
 */
public final class _733_FloodFillTest {
    @Test
    void test1() {
        var answer = floodFill(new int[][]{{0, 0, 0}, {0, 0, 0}}, 1, 0, 2);

        ArrayUtils.print(answer);

        Assertions.assertArrayEquals(new int[][]{{2, 2, 2}, {2, 2, 2}}, answer);
    }

    @Test
    void test2() {
        var answer = floodFill(new int[][]{{0, 0, 0}, {0, 0, 0}}, 0, 0, 0);

        ArrayUtils.print(answer);

        Assertions.assertArrayEquals(new int[][]{{0, 0, 0}, {0, 0, 0}}, answer);
    }

    @Test
    void test3() {
        var answer = floodFill(new int[][]{{1, 1, 1}, {1, 1, 0}, {1, 0, 1}}, 1, 1, 2);

        ArrayUtils.print(answer);

        Assertions.assertArrayEquals(new int[][]{{2, 2, 2}, {2, 2, 0}, {2, 0, 1}}, answer);
    }
}