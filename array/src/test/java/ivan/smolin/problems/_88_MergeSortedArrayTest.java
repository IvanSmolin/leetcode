package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._88_MergeSortedArray.merge;

/**
 * LeetCode Problem 88. Merge Sorted Array: https://leetcode.com/problems/merge-sorted-array/
 */
public final class _88_MergeSortedArrayTest {
    @Test
    void test1() {
        var answer = merge(new int[]{1, 2, 3, 0, 0, 0}, 3, new int[]{2, 5, 6}, 3);

        Assertions.assertArrayEquals(new int[]{1, 2, 2, 3, 5, 6}, answer);
    }

    @Test
    void test2() {
        var answer = merge(new int[]{4, 5, 6, 0, 0, 0}, 3, new int[]{1, 2, 3}, 3);

        Assertions.assertArrayEquals(new int[]{1, 2, 3, 4, 5, 6}, answer);
    }

    @Test
    void test3() {
        var answer = merge(new int[]{1}, 1, new int[]{}, 0);

        Assertions.assertArrayEquals(new int[]{1}, answer);
    }

    @Test
    void test4() {
        var answer = merge(new int[]{0}, 0, new int[]{1}, 1);

        Assertions.assertArrayEquals(new int[]{1}, answer);
    }
}