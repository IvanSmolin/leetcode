package ivan.smolin.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public final class BashUtils {
    public static void runBash(String command) throws IOException {
        command = "cd src/main/resources && " + command;
        var processBuilder = new ProcessBuilder("bash", "-c", command);
        var process = processBuilder.start();

        var reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line;

        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }
    }
}