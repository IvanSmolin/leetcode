package ivan.smolin.problems;

import java.io.IOException;

import static ivan.smolin.core.BashUtils.runBash;

/**
 * LeetCode Problem 193. Valid Phone Numbers: https://leetcode.com/problems/valid-phone-numbers/
 */
public final class _193_ValidPhoneNumbers {
    public static void validPhoneNumbers() throws IOException {
        runBash("grep -P '^(\\(\\d{3}\\) |\\d{3}-)\\d{3}-\\d{4}$' _193_ValidPhoneNumbers.txt");
    }
}