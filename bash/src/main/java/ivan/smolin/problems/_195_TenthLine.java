package ivan.smolin.problems;

import java.io.IOException;

import static ivan.smolin.core.BashUtils.runBash;

/**
 * LeetCode Problem 195. Tenth Line: https://leetcode.com/problems/tenth-line/
 */
public final class _195_TenthLine {
    public static void tenthLine() throws IOException {
        runBash("sed -n '10p' _195_TenthLine.txt");
    }
}