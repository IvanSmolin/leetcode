package ivan.smolin.problems;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static ivan.smolin.problems._193_ValidPhoneNumbers.validPhoneNumbers;

/**
 * LeetCode Problem 193. Valid Phone Numbers: https://leetcode.com/problems/valid-phone-numbers/
 */
public final class _193_ValidPhoneNumbersTest {
    @Test
    void test() throws IOException {
        validPhoneNumbers();
    }
}