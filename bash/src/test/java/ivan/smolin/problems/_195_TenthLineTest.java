package ivan.smolin.problems;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static ivan.smolin.problems._195_TenthLine.tenthLine;

/**
 * LeetCode Problem 195. Tenth Line: https://leetcode.com/problems/tenth-line/
 */
public final class _195_TenthLineTest {
    @Test
    void test() throws IOException {
        tenthLine();
    }
}