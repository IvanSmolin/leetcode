package ivan.smolin.problems;

/**
 * LeetCode Problem 190. Reverse Bits: https://leetcode.com/problems/reverse-bits/
 */
public final class _190_ReverseBits {
    public static int reverseBits(int n) {
        var result = 0;

        for (var i = 0; i < 32; i++) {
            result = (result << 1) | (n & 1);
            n >>= 1;
        }

        return result;
    }
}