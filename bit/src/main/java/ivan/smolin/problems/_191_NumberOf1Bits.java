package ivan.smolin.problems;

/**
 * LeetCode Problem 191. Number of 1 Bits: https://leetcode.com/problems/number-of-1-bits/
 */
public final class _191_NumberOf1Bits {
    public static int hammingWeight(int n) {
        var result = 0;

        while (n != 0) {
            result += n & 1;
            n >>>= 1;
        }

        return result;
    }
}