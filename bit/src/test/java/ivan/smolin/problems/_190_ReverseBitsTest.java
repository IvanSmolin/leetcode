package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._190_ReverseBits.reverseBits;

/**
 * LeetCode Problem 190. Reverse Bits: https://leetcode.com/problems/reverse-bits/
 */
public final class _190_ReverseBitsTest {
    @Test
    void test1() {
        var answer = reverseBits(43261596);

        Assertions.assertEquals(964176192, answer);
    }
}