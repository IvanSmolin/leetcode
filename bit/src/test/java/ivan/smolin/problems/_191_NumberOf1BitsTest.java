package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._191_NumberOf1Bits.hammingWeight;

/**
 * LeetCode Problem 191. Number of 1 Bits: https://leetcode.com/problems/number-of-1-bits/
 */
public final class _191_NumberOf1BitsTest {
    @Test
    void test1() {
        var answer = hammingWeight(11);

        Assertions.assertEquals(3, answer);
    }

    @Test
    void test2() {
        var answer = hammingWeight(128);

        Assertions.assertEquals(1, answer);
    }

    @Test
    void test3() {
        var answer = hammingWeight(-3);

        Assertions.assertEquals(31, answer);
    }
}