package ivan.smolin.problems;

import java.util.ArrayList;
import java.util.List;

/**
 * LeetCode Problem 118. Pascal's Triangle: https://leetcode.com/problems/pascals-triangle/
 */
public final class _118_PascalsTriangle {
    public static List<List<Integer>> generate(int numRows) {
        var result = new ArrayList<List<Integer>>();

        for (int i = 0; i < numRows; i++) {
            var row = new ArrayList<Integer>();

            for (int j = 0; j < i + 1; j++) {
                if (j == 0 || j == i) {
                    row.add(1);
                } else if (i > 1) {
                    row.add(result.get(i - 1).get(j - 1) + result.get(i - 1).get(j));
                }
            }

            result.add(row);
        }

        return result;
    }
}