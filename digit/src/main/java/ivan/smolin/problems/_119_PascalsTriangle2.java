package ivan.smolin.problems;

import java.util.ArrayList;
import java.util.List;

/**
 * LeetCode Problem 119. Pascal's Triangle II: https://leetcode.com/problems/pascals-triangle-ii/
 */
public final class _119_PascalsTriangle2 {
    public static List<Integer> getRow(int rowIndex) {
        var row = new ArrayList<Integer>();

        for (int i = 0; i <= rowIndex; i++) {
            row.add(1);

            for (int j = i - 1; j > 0; j--) {
                row.set(j, row.get(j) + row.get(j - 1));
            }
        }

        return row;
    }
}