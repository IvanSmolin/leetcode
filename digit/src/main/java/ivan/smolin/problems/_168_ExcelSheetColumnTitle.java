package ivan.smolin.problems;

/**
 * LeetCode Problem 168. Excel Sheet Column Title: https://leetcode.com/problems/excel-sheet-column-title/
 */
public final class _168_ExcelSheetColumnTitle {
    public static String convertToTitle(int columnNumber) {
        var result = new StringBuilder();
        var number = 0;

        while (columnNumber > 0) {
            number = columnNumber % 26;
            columnNumber /= 26;

            if (number == 0) {
                number = 26;
                columnNumber--;
            }

            result.insert(0, (char) ('A' + number - 1));
        }

        return result.toString();
    }
}