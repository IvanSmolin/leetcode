package ivan.smolin.problems;

/**
 * LeetCode Problem 202. Happy Number: https://leetcode.com/problems/happy-number/
 */
public final class _202_HappyNumber {
    /**
     * When calculating the sum of the squares of the digits of any number, we can get only two possible results: either
     * we get the number 1, or we enter an infinite loop that will consist only of the numbers 4.
     */
    public static boolean isHappy(int n) {
        do {
            n = getSquares(n);

            if (n == 1) {
                return true;
            }
        } while (n != 4);

        return false;
    }

    private static int getSquares(int n) {
        var result = 0;
        var buf = 0;

        while (n > 0) {
            buf = n % 10;
            result += buf * buf;
            n /= 10;
        }

        return result;
    }
}