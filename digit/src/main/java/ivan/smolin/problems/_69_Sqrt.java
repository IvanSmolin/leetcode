package ivan.smolin.problems;

/**
 * LeetCode Problem 69. Sqrt(x): https://leetcode.com/problems/sqrtx/
 */
public final class _69_Sqrt {
    public static int mySqrt(int x) {
        var left = 1;
        var right = x;

        while (left <= right) {
            var mid = left + (right - left) / 2;

            if (mid <= x / mid && (mid + 1) > x / (mid + 1)) {
                return mid;
            } else if (mid > x / mid) {
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }

        return 0;
    }
}