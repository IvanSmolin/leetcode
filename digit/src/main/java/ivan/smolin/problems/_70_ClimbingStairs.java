package ivan.smolin.problems;

/**
 * LeetCode Problem 70. Climbing Stairs: https://leetcode.com/problems/climbing-stairs/
 */
public final class _70_ClimbingStairs {
    public static int climbStairs(int n) {
        var a = 0;
        var b = 1;
        var sum = 0;

        for (int i = 0; i < n; i++) {
            sum = a + b;
            a = b;
            b = sum;
        }

        return sum;
    }
}