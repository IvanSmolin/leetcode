package ivan.smolin.problems;

import java.util.ArrayList;
import java.util.List;

/**
 * LeetCode Problem 728. Self Dividing Numbers: https://leetcode.com/problems/self-dividing-numbers/
 */
public final class _728_SelfDividingNumbers {
    public static List<Integer> selfDividingNumbers(int left, int right) {
        List<Integer> result = new ArrayList<>(right - left);

        for (int i = left; i <= right; i++) {
            if (isDividingNumber(i)) {
                result.add(i);
            }
        }

        return result;
    }

    public static boolean isDividingNumber(int number) {
        int num = number;
        int n;

        while (num != 0) {
            n = num % 10;

            if (n == 0 || number % n != 0) {
                return false;
            }

            num /= 10;
        }

        return true;
    }
}