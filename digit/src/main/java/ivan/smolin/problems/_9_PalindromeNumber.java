package ivan.smolin.problems;

/**
 * LeetCode Problem 9. Palindrome Number: https://leetcode.com/problems/palindrome-number/
 */
public final class _9_PalindromeNumber {
    public static boolean isPalindrome(int x) {
        if (x < 0) {
            return false;
        }

        int result = 0;
        int val = x;

        while (val != 0) {
            result *= 10;
            result += val % 10;

            val /= 10;
        }

        return x == result;
    }
}