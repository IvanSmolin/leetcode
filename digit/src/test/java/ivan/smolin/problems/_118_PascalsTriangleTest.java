package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static ivan.smolin.problems._118_PascalsTriangle.generate;

/**
 * LeetCode Problem 118. Pascal's Triangle: https://leetcode.com/problems/pascals-triangle/
 */
public final class _118_PascalsTriangleTest {
    @Test
    void test1() {
        var expected = List.of(
            List.of(1),
            List.of(1, 1),
            List.of(1, 2, 1),
            List.of(1, 3, 3, 1),
            List.of(1, 4, 6, 4, 1)
        );

        var actual = generate(5);

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void test2() {
        var expected = List.of(
            List.of(1)
        );

        var actual = generate(1);

        Assertions.assertEquals(expected, actual);
    }
}