package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static ivan.smolin.problems._119_PascalsTriangle2.getRow;

/**
 * LeetCode Problem 119. Pascal's Triangle II: https://leetcode.com/problems/pascals-triangle-ii/
 */
public final class _119_PascalsTriangle2Test {
    @Test
    void test1() {
        var expected = List.of(1, 3, 3, 1);

        var actual = getRow(3);

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void test2() {
        var expected = List.of(1);

        var actual = getRow(0);

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void test3() {
        var expected = List.of(1, 1);

        var actual = getRow(1);

        Assertions.assertEquals(expected, actual);
    }
}