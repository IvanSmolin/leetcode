package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._168_ExcelSheetColumnTitle.convertToTitle;

/**
 * LeetCode Problem 168. Excel Sheet Column Title: https://leetcode.com/problems/excel-sheet-column-title/
 */
public final class _168_ExcelSheetColumnTitleTest {
    @Test
    void test1() {
        var answer = convertToTitle(1);

        Assertions.assertEquals("A", answer);
    }

    @Test
    void test2() {
        var answer = convertToTitle(28);

        Assertions.assertEquals("AB", answer);
    }

    @Test
    void test3() {
        var answer = convertToTitle(701);

        Assertions.assertEquals("ZY", answer);
    }

    @Test
    void test4() {
        var answer = convertToTitle(2367485);

        Assertions.assertEquals("EDREC", answer);
    }
}