package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._202_HappyNumber.isHappy;

/**
 * LeetCode Problem 202. Happy Number: https://leetcode.com/problems/happy-number/
 */
public final class _202_HappyNumberTest {
    @Test
    void test1() {
        Assertions.assertTrue(isHappy(19));
    }

    @Test
    void test2() {
        Assertions.assertFalse(isHappy(2));
    }

    @Test
    void test3() {
        Assertions.assertFalse(isHappy(4));
    }
}