package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._69_Sqrt.mySqrt;

/**
 * LeetCode Problem 69. Sqrt(x): https://leetcode.com/problems/sqrtx/
 */
public final class _69_SqrtTest {
    @Test
    void test1() {
        var answer = mySqrt(4);

        Assertions.assertEquals(2, answer);
    }

    @Test
    void test2() {
        var answer = mySqrt(8);

        Assertions.assertEquals(2, answer);
    }
}