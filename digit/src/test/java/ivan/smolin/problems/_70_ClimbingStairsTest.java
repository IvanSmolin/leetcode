package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._70_ClimbingStairs.climbStairs;

/**
 * LeetCode Problem 70. Climbing Stairs: https://leetcode.com/problems/climbing-stairs/
 */
public final class _70_ClimbingStairsTest {
    @Test
    void test1() {
        var answer = climbStairs(2);

        Assertions.assertEquals(2, answer);
    }

    @Test
    void test2() {
        var answer = climbStairs(3);

        Assertions.assertEquals(3, answer);
    }

    @Test
    void test3() {
        var answer = climbStairs(4);

        Assertions.assertEquals(5, answer);
    }

    @Test
    void test4() {
        var answer = climbStairs(5);

        Assertions.assertEquals(8, answer);
    }

    @Test
    void test5() {
        var answer = climbStairs(10);

        Assertions.assertEquals(89, answer);
    }
}