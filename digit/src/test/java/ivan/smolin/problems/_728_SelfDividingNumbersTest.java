package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static ivan.smolin.problems._728_SelfDividingNumbers.selfDividingNumbers;

/**
 * LeetCode Problem 728. Self Dividing Numbers: https://leetcode.com/problems/self-dividing-numbers/
 */
public final class _728_SelfDividingNumbersTest {
    @Test
    void test1() {
        List<Integer> answer = selfDividingNumbers(1, 22);

        Assertions.assertEquals(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 15, 22), answer);
    }

    @Test
    void test2() {
        List<Integer> answer = selfDividingNumbers(47, 85);

        Assertions.assertEquals(Arrays.asList(48, 55, 66, 77), answer);
    }
}