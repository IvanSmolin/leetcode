package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._9_PalindromeNumber.isPalindrome;

/**
 * LeetCode Problem 9. Palindrome Number: https://leetcode.com/problems/palindrome-number/
 */
public final class _9_PalindromeNumberTest {
    @Test
    void test1() {
        boolean answer = isPalindrome(121);

        Assertions.assertTrue(answer);
    }

    @Test
    void test2() {
        boolean answer = isPalindrome(-121);

        Assertions.assertFalse(answer);
    }

    @Test
    void test3() {
        boolean answer = isPalindrome(10);

        Assertions.assertFalse(answer);
    }
}