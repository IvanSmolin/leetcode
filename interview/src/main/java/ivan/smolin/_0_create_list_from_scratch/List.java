package ivan.smolin._0_create_list_from_scratch;

public final class List {
    private Node first;

    private Node last;

    public void addFirst(String value) {
        if (first == null) {
            first = new Node(value);
            last = first;
        } else {
            if (last == first) {
                last = new Node(value);
                last.next = first;
                first = last;
            } else {
                Node node = new Node(value);
                node.next = first;
                first = node;
            }
        }
    }

    public void addLast(String value) {
        if (first == null) {
            first = new Node(value);
            last = first;
        } else {
            if (last == first) {
                last = new Node(value);
                first.next = last;
            } else {
                Node node = new Node(value);
                last.next = node;
                last = node;
            }
        }
    }

    public void deleteFirst() {
        first = first.next;
    }

    public void deleteLast() {
        Node node = first;

        while (node.next != last) {
            node = node.next;
        }

        node.next = null;
        last = node;
    }

    public void delete(String value) {
        Node node = first;

        while (node != null) {
            if (node.next == null && node.value.equals(value)) {
                first = null;
                return;
            } else if (node.value.equals(value)) {
                first = first.next;
                return;
            }

            if (node.next != null && node.next.value.equals(value)) {
                if (node.next == last) {
                    node.next = null;
                    last = node;
                    return;
                }

                node.next = node.next.next;
                return;
            }

            node = node.next;
        }
    }

    public void print() {
        StringBuffer level = new StringBuffer("");
        Node node = first;

        if (node == null) {
            System.out.println("List is empty");
        }

        while (node != null) {
            System.out.println(level.append(".") + node.value);
            node = node.next;
        }
    }
}