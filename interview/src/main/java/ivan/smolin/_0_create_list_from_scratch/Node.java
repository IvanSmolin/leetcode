package ivan.smolin._0_create_list_from_scratch;

public final class Node {
    public String value;

    public Node next;

    Node(String value) {
        this.value = value;
    }
}