package ivan.smolin._1_baku_bank;

public final class Fibonacci {
    public static int fibonacciRecursion(int count) {
        if (count == 0) {
            return 0;
        } else if (count == 1) {
            return 1;
        } else {
            return fibonacci(count - 1) + fibonacci(count - 2);
        }
    }

    public static int fibonacci(int count) {
        var n1 = 0;
        var n2 = 1;
        var result = 0;

        for (int i = 0; i < count; i++) {
            result = n1 + n2;
            n1 = n2;
            n2 = result;
        }

        return result;
    }
}