package ivan.smolin._2_deutsche_bank;

public final class Dice {
    public static int dice() {
        return (int) (Math.random() * 6 + 1);
    }
}