package ivan.smolin._3_pimco;

import java.util.List;

public final class GetMax {
    public static int getMax(List<Integer> array) {
        if (array == null || array.isEmpty()) {
            throw new IllegalArgumentException();
        }

        int max = array.get(0);

        for (int i = 1; i < array.size(); i++) {
            if (array.get(i) > max) {
                max = array.get(i);
            }
        }

        return max;
    }
}