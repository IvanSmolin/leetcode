package ivan.smolin._3_pimco;

import java.util.List;

public final class GetTwoMax {
    public static int[] getTwoMax(List<Integer> array) {
        if (array == null || array.isEmpty() || array.size() < 2) {
            throw new IllegalArgumentException();
        }

        int max1 = array.get(0);
        int max2 = array.get(1);

        for (int i = 2; i < array.size(); i++) {
            if (array.get(i) > max1) {
                max2 = max1;
                max1 = array.get(i);
            } else if (array.get(i) > max2) {
                max2 = array.get(i);
            }
        }

        return new int[]{max1, max2};
    }
}