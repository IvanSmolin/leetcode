package ivan.smolin._4_vonage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public final class _0_CodeChallenge {
    public static int minLettersToRemove(String s) {
        var map = new HashMap<Character, Integer>();
        var result = 0;

        for (var i = 0; i < s.length(); i++) {
            map.compute(s.charAt(i), (key, value) -> value == null ? 1 : ++value);
        }

        for (var i : map.values()) {
            if (i % 2 == 1) {
                result++;
            }
        }

        return result == 0 ? 0 : --result;
    }

    public static String[] solution(int i, int k) {
        if (i == 0) {
            return new String[]{""};
        }

        ArrayList<String> result = new ArrayList<>();

        for (String p : solution(i - 1, k - 1)) {
            for (char l : new char[]{'a', 'b', 'c'}) {
                int len = p.length();
                if (len == 0 || p.charAt(len - 1) != l) {
                    result.add(p + l);
                }
            }
        }

        Collections.sort(result);
        int prefSize = Math.min(result.size(), k);

        return result.subList(0, prefSize).toArray(new String[prefSize]);
    }
}