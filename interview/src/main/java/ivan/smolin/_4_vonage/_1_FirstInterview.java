package ivan.smolin._4_vonage;

public final class _1_FirstInterview {
    public static String dnaEncoding(String str) {
        var result = new StringBuilder();
        var buf = str.charAt(0);
        var num = 1;

        for (int i = 1; i < str.length(); i++) {
            if (buf == str.charAt(i)) {
                num++;
            } else {
                if (num > 1) {
                    result.append(num);
                }

                result.append(buf);

                buf = str.charAt(i);
                num = 1;
            }
        }

        if (num > 1) {
            result.append(num);
        }

        result.append(buf);

        return result.toString();
    }
}