package ivan.smolin._4_vonage;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class _2_SecondInterview {
    public static boolean isUnique(List<Integer> args) {
        Set<Integer> result = new HashSet<>(args);

        return result.size() == args.size();
    }

    public static List<Integer> getFromPerfectNumber(int number) {
        List<Integer> result = new ArrayList<>(List.of(1));
        int sum = 1;

        for (int i = 2; i <= number / 2; i++) {
            if (number % i == 0) {
                sum += i;
                result.add(i);
            }
        }

        if (sum == number) {
            return result;
        } else {
            throw new IllegalArgumentException();
        }
    }
}