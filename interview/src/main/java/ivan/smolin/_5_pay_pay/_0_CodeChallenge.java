package ivan.smolin._5_pay_pay;

import java.util.Arrays;
import java.util.HashSet;

public final class _0_CodeChallenge {
    public static boolean solution1(String[] words, String variableName) {
        var wordSet = new HashSet<>(Arrays.asList(words));

        var nameWords = variableName.split("(?=[A-Z])");

        for (String word : nameWords) {
            if (!wordSet.contains(word.toLowerCase())) {
                return false;
            }
        }

        return true;
    }

    public static String solution2(String[] arr) {
        var result = new StringBuilder();
        var maxLen = getMaxLen(arr);

        for (int i = 0; i < maxLen; i++) {
            for (String s : arr) {
                if (i < s.length()) {
                    result.append(s.charAt(i));
                }
            }
        }

        return result.toString();
    }

    private static int getMaxLen(String[] arr) {
        var max = 0;

        for (String s : arr) {
            if (s.length() > max) {
                max = s.length();
            }
        }

        return max;
    }
}