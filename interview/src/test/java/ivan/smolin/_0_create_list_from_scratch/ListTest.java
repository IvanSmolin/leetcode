package ivan.smolin._0_create_list_from_scratch;

import org.junit.jupiter.api.Test;

public final class ListTest {
    @Test
    void test() {
        var list = new List();

        for (int i = 0; i <= 5; i++) {
            list.addLast(String.valueOf(i));
        }

        for (int i = 10; i <= 15; i++) {
            list.addFirst(String.valueOf(i));
        }

        list.deleteFirst();
        list.deleteLast();

        list.print();

        list.deleteFirst();
        list.deleteLast();

        list.print();

        list.delete("0");

        list.print();

        list.delete("2");
        list.delete("12");

        list.print();

        list.delete("13");
        list.delete("10");
        list.delete("3");
        list.delete("1");

        list.print();

        list.delete("13");
        list.delete("10");
        list.delete("3");
        list.delete("1");

        list.print();

        list.delete("11");

        list.print();
    }
}