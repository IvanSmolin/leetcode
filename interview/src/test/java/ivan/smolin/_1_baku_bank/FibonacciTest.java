package ivan.smolin._1_baku_bank;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin._1_baku_bank.Fibonacci.fibonacci;
import static ivan.smolin._1_baku_bank.Fibonacci.fibonacciRecursion;

public final class FibonacciTest {
    @Test
    void test1() {
        var answer = fibonacciRecursion(10);

        Assertions.assertEquals(89, answer);
    }

    @Test
    void test2() {
        var answer = fibonacciRecursion(11);

        Assertions.assertEquals(144, answer);
    }

    @Test
    void test3() {
        var answer = fibonacci(10);

        Assertions.assertEquals(89, answer);
    }

    @Test
    void test4() {
        var answer = fibonacci(11);

        Assertions.assertEquals(144, answer);
    }
}