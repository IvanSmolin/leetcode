package ivan.smolin._2_deutsche_bank;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static ivan.smolin._2_deutsche_bank.Dice.dice;

public final class DiceTest {
    @Test
    void test1() {
        int iterations = 1000000;

        var map = new HashMap<Integer, Integer>();

        for (int i = 0; i < iterations; i++) {
            var answer = dice();

            map.put(answer, map.getOrDefault(answer, 0) + 1);
        }

        var avarage = iterations / 6;
        var max = avarage * 1.01;
        var min = avarage * 0.99;

        System.out.println("Min value: " + min + "\nMax value: " + max);

        map.forEach((key, value) -> {
                System.out.println(key + ": " + value);
                Assertions.assertTrue(key >= 1 && key <= 6);
                Assertions.assertTrue(value >= min && value <= max);
            }
        );
    }
}