package ivan.smolin._3_pimco;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static ivan.smolin._3_pimco.GetMax.getMax;

public final class GetMaxTest {
    @Test
    public void test1() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> getMax(null));
    }

    @Test
    public void test2() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> getMax(List.of()));
    }

    @Test
    public void test3() {
        Assertions.assertEquals(1, getMax(List.of(1)));
    }

    @Test
    public void test4() {
        Assertions.assertEquals(4, getMax(List.of(1, 4)));
    }

    @Test
    public void test5() {
        Assertions.assertEquals(45, getMax(List.of(45, -1, 8, 2, 4, 6)));
    }
}