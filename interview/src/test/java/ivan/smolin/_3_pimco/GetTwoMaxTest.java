package ivan.smolin._3_pimco;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static ivan.smolin._3_pimco.GetTwoMax.getTwoMax;

public final class GetTwoMaxTest {
    @Test
    public void test1() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> getTwoMax(null));
    }

    @Test
    public void test2() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> getTwoMax(List.of()));
    }

    @Test
    public void test3() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> getTwoMax(List.of(1)));
    }

    @Test
    public void test4() {
        Assertions.assertArrayEquals(new int[]{1, 4}, getTwoMax(List.of(1, 4)));
    }

    @Test
    public void test5() {
        Assertions.assertArrayEquals(new int[]{45, 8}, getTwoMax(List.of(45, -1, 8, 2, 4, 6)));
    }
}