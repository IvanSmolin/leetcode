package ivan.smolin._4_vonage;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin._4_vonage._0_CodeChallenge.minLettersToRemove;
import static ivan.smolin._4_vonage._0_CodeChallenge.solution;

public final class _0_CodeChallengeTest {
    @Test
    void test1() {
        Assertions.assertEquals(2, minLettersToRemove("ervervige"));
    }

    @Test
    void test2() {
        Assertions.assertEquals(0, minLettersToRemove("aaabab"));
    }

    @Test
    void test3() {
        Assertions.assertEquals(0, minLettersToRemove("x"));
    }

    @Test
    void test4() {
        String[] expected = {
            "ababababababababababababababababababababababababab",
            "ababababababababababababababababababababababababac",
            "ababababababababababababababababababababababababca",
            "ababababababababababababababababababababababababcb",
            "abababababababababababababababababababababababacab",
            "abababababababababababababababababababababababacac",
            "abababababababababababababababababababababababacba",
            "abababababababababababababababababababababababacbc",
            "abababababababababababababababababababababababcaba",
            "abababababababababababababababababababababababcabc",
            "abababababababababababababababababababababababcaca",
            "abababababababababababababababababababababababcacb",
            "abababababababababababababababababababababababcbab",
            "abababababababababababababababababababababababcbac",
            "abababababababababababababababababababababababcbca",
            "abababababababababababababababababababababababcbcb",
            "ababababababababababababababababababababababacabab",
            "ababababababababababababababababababababababacabac",
            "ababababababababababababababababababababababacabca",
            "ababababababababababababababababababababababacabcb",
            "ababababababababababababababababababababababacacab",
            "ababababababababababababababababababababababacacac",
            "ababababababababababababababababababababababacacba",
            "ababababababababababababababababababababababacacbc",
            "ababababababababababababababababababababababacbaba",
            "ababababababababababababababababababababababacbabc",
            "ababababababababababababababababababababababacbaca",
            "ababababababababababababababababababababababacbacb",
            "ababababababababababababababababababababababacbcab",
            "ababababababababababababababababababababababacbcac",
            "ababababababababababababababababababababababacbcba",
            "ababababababababababababababababababababababacbcbc",
            "ababababababababababababababababababababababcababa",
            "ababababababababababababababababababababababcababc",
            "ababababababababababababababababababababababcabaca",
            "ababababababababababababababababababababababcabacb",
            "ababababababababababababababababababababababcabcab",
            "ababababababababababababababababababababababcabcac",
            "ababababababababababababababababababababababcabcba",
            "ababababababababababababababababababababababcabcbc",
            "ababababababababababababababababababababababcacaba",
            "ababababababababababababababababababababababcacabc",
            "ababababababababababababababababababababababcacaca",
            "ababababababababababababababababababababababcacacb",
            "ababababababababababababababababababababababcacbab",
            "ababababababababababababababababababababababcacbac",
            "ababababababababababababababababababababababcacbca",
            "ababababababababababababababababababababababcacbcb",
            "ababababababababababababababababababababababcbabab",
            "ababababababababababababababababababababababcbabac",
            "ababababababababababababababababababababababcbabca",
            "ababababababababababababababababababababababcbabcb",
            "ababababababababababababababababababababababcbacab",
            "ababababababababababababababababababababababcbacac",
            "ababababababababababababababababababababababcbacba",
            "ababababababababababababababababababababababcbacbc",
            "ababababababababababababababababababababababcbcaba",
            "ababababababababababababababababababababababcbcabc",
            "ababababababababababababababababababababababcbcaca",
            "ababababababababababababababababababababababcbcacb",
            "ababababababababababababababababababababababcbcbab",
            "ababababababababababababababababababababababcbcbac",
            "ababababababababababababababababababababababcbcbca",
            "ababababababababababababababababababababababcbcbcb",
            "abababababababababababababababababababababacababab",
            "abababababababababababababababababababababacababac",
            "abababababababababababababababababababababacababca",
            "abababababababababababababababababababababacababcb",
            "abababababababababababababababababababababacabacab",
            "abababababababababababababababababababababacabacac",
            "abababababababababababababababababababababacabacba",
            "abababababababababababababababababababababacabacbc",
            "abababababababababababababababababababababacabcaba",
            "abababababababababababababababababababababacabcabc",
            "abababababababababababababababababababababacabcaca",
            "abababababababababababababababababababababacabcacb",
            "abababababababababababababababababababababacabcbab",
            "abababababababababababababababababababababacabcbac",
            "abababababababababababababababababababababacabcbca",
            "abababababababababababababababababababababacabcbcb",
            "abababababababababababababababababababababacacabab",
            "abababababababababababababababababababababacacabac",
            "abababababababababababababababababababababacacabca",
            "abababababababababababababababababababababacacabcb",
            "abababababababababababababababababababababacacacab",
            "abababababababababababababababababababababacacacac",
            "abababababababababababababababababababababacacacba",
            "abababababababababababababababababababababacacacbc",
            "abababababababababababababababababababababacacbaba",
            "abababababababababababababababababababababacacbabc"
        };

        Assertions.assertArrayEquals(expected, solution(50, 90));
    }
}