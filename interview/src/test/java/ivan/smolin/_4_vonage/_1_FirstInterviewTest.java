package ivan.smolin._4_vonage;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin._4_vonage._1_FirstInterview.dnaEncoding;

public final class _1_FirstInterviewTest {
    @Test
    public void test1() {
        Assertions.assertEquals("A2B3CD4EA", dnaEncoding("ABBCCCDEEEEA"));
    }

    @Test
    public void test2() {
        Assertions.assertEquals("A2B3CD4E3A", dnaEncoding("ABBCCCDEEEEAAA"));
    }
}