package ivan.smolin._4_vonage;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static ivan.smolin._4_vonage._2_SecondInterview.getFromPerfectNumber;
import static ivan.smolin._4_vonage._2_SecondInterview.isUnique;

public final class _2_SecondInterviewTest {
    @Test
    public void test1() {
        Assertions.assertTrue(isUnique(List.of(1, 2, 3)));
    }

    @Test
    public void test2() {
        Assertions.assertFalse(isUnique(List.of(1, 2, 3, 3)));
    }

    @Test
    public void test3() {
        Assertions.assertTrue(isUnique(List.of(1)));
    }

    @Test
    public void test4() {
        Assertions.assertTrue(isUnique(List.of()));
    }

    @Test
    public void test5() {
        Assertions.assertEquals(List.of(1, 2, 3), getFromPerfectNumber(6));
    }

    @Test
    public void test6() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> getFromPerfectNumber(7));
    }
}