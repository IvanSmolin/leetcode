package ivan.smolin._5_pay_pay;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin._5_pay_pay._0_CodeChallenge.solution1;
import static ivan.smolin._5_pay_pay._0_CodeChallenge.solution2;

public final class _0_CodeChallengeTest {
    @Test
    void test1() {
        Assertions.assertTrue(solution1(new String[]{"is", "valid", "right"}, "isValid"));
    }

    @Test
    void test2() {
        Assertions.assertFalse(solution1(new String[]{"is", "valid", "right"}, "isVal1d"));
    }

    @Test
    void test3() {
        Assertions.assertEquals(
            "DRHPaoyoisapsecpyiynth",
            solution2(new String[]{"Daisy", "Rose", "Hyacinth", "Poppy"})
        );
    }
}