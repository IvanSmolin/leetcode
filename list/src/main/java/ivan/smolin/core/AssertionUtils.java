package ivan.smolin.core;

import ivan.smolin.core.ListUtils.ListNode;
import org.junit.jupiter.api.Assertions;

public final class AssertionUtils {
    public static void assertion(int[] expected, ListNode answer) {
        answer.print();

        for (int value : expected) {
            Assertions.assertEquals(value, answer.val);
            answer = answer.next;
        }
    }

    public static void assertion(ListNode expected, ListNode actual) {
        while (actual != null) {
            Assertions.assertEquals(expected.val, actual.val);

            actual = actual.next;
            expected = expected.next;
        }
    }
}