package ivan.smolin.core;

import java.util.ArrayList;
import java.util.List;

public final class ListUtils {
    public static ListNode createList(int length) {
        var head = new ListNode(1);
        var end = head;

        for (int i = 2; i <= length; i++) {
            end.next = new ListNode(i);
            end = end.next;
        }

        head.print();

        return head;
    }

    public static ListNode createList(int[] values) {
        ListNode head = null;
        ListNode current = null;

        for (int value : values) {
            if (head == null) {
                head = new ListNode(value);
                current = head;
            } else {
                var newNode = new ListNode(value);
                current.next = newNode;
                current = newNode;
            }
        }

        return head;
    }

    public static ListNode createCycledList(int[] list, int cycleStart) {
        ListNode head = null;
        ListNode current = null;
        ListNode tailStart = null;
        int length = 0;

        for (int value : list) {
            if (head == null) {
                head = new ListNode(value);
                current = head;
            } else {
                ListNode newNode = new ListNode(value);
                current.next = newNode;
                current = newNode;
            }

            if (length == cycleStart) {
                tailStart = current;
            }

            length++;

            if (length == list.length) {
                current.next = tailStart;
            }
        }

        return head;
    }

    public static class ListNode {
        public int val;

        public ListNode next;

        public ListNode() {
        }

        public ListNode(int val) {
            this.val = val;
        }

        public ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }

        public void print() {
            var end = this;

            while (end != null) {
                System.out.print(end.val + ", ");
                end = end.next;
            }
        }

        public List<Integer> getLast() {
            var end = this;
            var result = new ArrayList<Integer>();

            while (end != null) {
                result.add(end.val);
                end = end.next;
            }

            return result;
        }
    }
}