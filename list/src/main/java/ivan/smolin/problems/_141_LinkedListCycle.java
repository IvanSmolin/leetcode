package ivan.smolin.problems;

import ivan.smolin.core.ListUtils.ListNode;

/**
 * LeetCode Problem 141. Linked List Cycle: https://leetcode.com/problems/linked-list-cycle/
 */
public final class _141_LinkedListCycle {
    public static boolean hasCycle(ListNode head) {
        var firstIterator = head;
        var secondIterator = head;

        while (secondIterator != null && secondIterator.next != null) {
            firstIterator = firstIterator.next;
            secondIterator = secondIterator.next.next;

            if (firstIterator == secondIterator) {
                return true;
            }
        }

        return false;
    }
}