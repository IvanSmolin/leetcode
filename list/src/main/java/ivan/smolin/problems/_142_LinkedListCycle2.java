package ivan.smolin.problems;

import ivan.smolin.core.ListUtils.ListNode;

/**
 * LeetCode Problem 142. Linked List Cycle II: https://leetcode.com/problems/linked-list-cycle-ii/
 */
public final class _142_LinkedListCycle2 {
    public static ListNode detectCycle(ListNode head) {
        var firstIterator = head;
        var secondIterator = head;

        while (true) {
            if (secondIterator != null && secondIterator.next != null) {
                firstIterator = firstIterator.next;
                secondIterator = secondIterator.next.next;
            } else {
                return null;
            }

            if (firstIterator == secondIterator) {
                firstIterator = head;

                while (firstIterator != secondIterator) {
                    firstIterator = firstIterator.next;
                    secondIterator = secondIterator.next;
                }

                return firstIterator;
            }
        }
    }
}