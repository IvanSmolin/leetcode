package ivan.smolin.problems;

import ivan.smolin.core.ListUtils.ListNode;

import java.util.HashMap;

/**
 * LeetCode Problem 160. Intersection of Two Linked Lists:
 * https://leetcode.com/problems/intersection-of-two-linked-lists/
 */
public final class _160_IntersectionOfTwoLinkedLists {
    public static ListNode getIntersectionNode1(ListNode headA, ListNode headB) {
        var map = new HashMap<ListNode, Integer>();

        while (headA != null || headB != null) {
            if (headA != null) {
                map.put(headA, map.getOrDefault(headA, 0) + 1);

                if (map.get(headA) == 2) {
                    return headA;
                }

                headA = headA.next;
            }

            if (headB != null) {
                map.put(headB, map.getOrDefault(headB, 0) + 1);

                if (map.get(headB) == 2) {
                    return headB;
                }

                headB = headB.next;
            }
        }

        return null;
    }

    public static ListNode getIntersectionNode2(ListNode headA, ListNode headB) {
        var lengthA = getLength(headA);
        var lengthB = getLength(headB);

        while (lengthA != lengthB) {
            if (lengthA > lengthB) {
                headA = headA.next;
                lengthA--;
            } else {
                headB = headB.next;
                lengthB--;
            }
        }

        while (headA != null) {
            if (headA == headB) {
                return headA;
            }

            headA = headA.next;
            headB = headB.next;
        }

        return null;
    }

    private static int getLength(ListNode head) {
        var length = 0;

        while (head != null) {
            length++;
            head = head.next;
        }

        return length;
    }
}