package ivan.smolin.problems;

import ivan.smolin.core.ListUtils.ListNode;

/**
 * LeetCode Problem 203. Remove Linked List Elements: https://leetcode.com/problems/remove-linked-list-elements/
 */
public final class _203_RemoveLinkedListElements {
    public static ListNode removeElements(ListNode head, int val) {
        ListNode list = head;
        ListNode previousNode = null;

        while (list != null) {
            if (list.val == val) {
                if (previousNode == null) {
                    list = list.next;
                    head = list;
                } else {
                    previousNode.next = list.next;
                    list = list.next;
                }
            } else {
                previousNode = list;
                list = list.next;
            }
        }

        return head;
    }
}