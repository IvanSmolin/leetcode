package ivan.smolin.problems;

import ivan.smolin.core.ListUtils.ListNode;

/**
 * LeetCode Problem 206. Reverse Linked List: https://leetcode.com/problems/reverse-linked-list/
 */
public final class _206_ReverseLinkedList {
    public static ListNode reverseList(ListNode head) {
        ListNode previous = null;
        var next = head;

        while (next != null) {
            next = head.next;
            head.next = previous;
            previous = head;
            head = next;
        }

        return previous;
    }
}