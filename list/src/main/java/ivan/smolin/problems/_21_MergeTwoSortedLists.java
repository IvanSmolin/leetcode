package ivan.smolin.problems;

import ivan.smolin.core.ListUtils.ListNode;

/**
 * LeetCode Problem 21. Merge Two Sorted Lists: https://leetcode.com/problems/merge-two-sorted-lists/
 */
public final class _21_MergeTwoSortedLists {
    public static ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        var result = new ListNode();
        var node = result;

        while (list1 != null && list2 != null) {
            if (list1.val < list2.val) {
                node.next = list1;
                list1 = list1.next;
            } else {
                node.next = list2;
                list2 = list2.next;
            }

            node = node.next;
        }

        if (list1 == null) {
            node.next = list2;
        } else {
            node.next = list1;
        }

        return result.next;
    }
}