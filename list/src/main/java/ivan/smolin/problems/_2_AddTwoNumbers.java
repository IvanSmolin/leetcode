package ivan.smolin.problems;

import ivan.smolin.core.ListUtils.ListNode;

/**
 * LeetCode Problem 2. Add Two Numbers: https://leetcode.com/problems/add-two-numbers/
 */
public final class _2_AddTwoNumbers {
    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode head = null;
        ListNode result = null;
        int increment = 0;

        while (l1 != null || l2 != null) {
            int sum = increment;

            if (l1 != null) {
                sum += l1.val;
                l1 = l1.next;
            }

            if (l2 != null) {
                sum += l2.val;
                l2 = l2.next;
            }

            ListNode node = new ListNode(sum % 10);
            increment = sum / 10;

            if (head == null) {
                head = node;
                result = node;
            } else {
                result.next = node;
                result = node;
            }
        }

        if (increment > 0) {
            result.next = new ListNode(increment);
        }

        return head;
    }
}