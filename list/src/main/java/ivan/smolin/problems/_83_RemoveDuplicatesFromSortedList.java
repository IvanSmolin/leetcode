package ivan.smolin.problems;

import ivan.smolin.core.ListUtils.ListNode;

/**
 * LeetCode Problem 83. Remove Duplicates from Sorted List:
 * https://leetcode.com/problems/remove-duplicates-from-sorted-list/
 */
public final class _83_RemoveDuplicatesFromSortedList {
    public static ListNode deleteDuplicates(ListNode head) {
        var result = head;

        if (result == null) {
            return null;
        }

        while (head.next != null) {
            if (head.val == head.next.val) {
                head.next = head.next.next;
            } else {
                head = head.next;
            }
        }

        return result;
    }
}