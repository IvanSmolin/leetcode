package ivan.smolin.problems;

import ivan.smolin.core.ListUtils.ListNode;

/**
 * LeetCode Problem 876. Middle of the Linked List: https://leetcode.com/problems/middle-of-the-linked-list/
 */
public final class _876_MiddleOfTheLinkedList {
    public static ListNode middleNode(ListNode head) {
        ListNode fast = head;

        while (fast != null && fast.next != null) {
            head = head.next;
            fast = fast.next.next;
        }

        return head;
    }
}