package ivan.smolin.problems;

import ivan.smolin.core.ListUtils.ListNode;

/**
 * LeetCode Problem 92. Reverse Linked List II: https://leetcode.com/problems/reverse-linked-list-ii/
 */
public final class _92_ReverseLinkedList2 {
    public static ListNode reverseBetween(ListNode head, int left, int right) {
        var result = new ListNode();
        result.next = head;
        var head1 = result;

        for (var i = 1; i < left; i++) {
            head1 = head1.next;
        }

        ListNode head2 = head1.next;
        ListNode list = head2;
        ListNode previous = null;
        ListNode buf;

        for (var i = left; i <= right; i++) {
            buf = list.next;
            list.next = previous;
            previous = list;
            list = buf;
        }

        head1.next = previous;
        head2.next = list;

        return result.next;
    }
}