package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.core.ListUtils.createCycledList;
import static ivan.smolin.problems._141_LinkedListCycle.hasCycle;

/**
 * LeetCode Problem 141. Linked List Cycle: https://leetcode.com/problems/linked-list-cycle/
 */
public final class _141_LinkedListCycleTest {
    @Test
    void test1() {
        boolean answer = hasCycle(createCycledList(new int[]{3, 2, 0, -4}, 1));

        Assertions.assertTrue(answer);
    }

    @Test
    void test2() {
        boolean answer = hasCycle(createCycledList(new int[]{1, 2}, 0));

        Assertions.assertTrue(answer);
    }

    @Test
    void test3() {
        boolean answer = hasCycle(createCycledList(new int[]{1}, -1));

        Assertions.assertFalse(answer);
    }

    @Test
    void test4() {
        boolean answer = hasCycle(createCycledList(new int[]{1, 2}, -1));

        Assertions.assertFalse(answer);
    }
}