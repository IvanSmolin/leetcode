package ivan.smolin.problems;

import ivan.smolin.core.ListUtils.ListNode;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.core.ListUtils.createCycledList;
import static ivan.smolin.problems._142_LinkedListCycle2.detectCycle;

/**
 * LeetCode Problem 142. Linked List Cycle II: https://leetcode.com/problems/linked-list-cycle-ii/
 */
public final class _142_LinkedListCycle2Test {
    @Test
    void test1() {
        ListNode list = createCycledList(new int[]{3, 2, 0, -4}, 1);
        var answer = detectCycle(list);

        Assertions.assertEquals(list.next, answer);
    }

    @Test
    void test2() {
        ListNode list = createCycledList(new int[]{1, 2}, 0);
        var answer = detectCycle(list);

        Assertions.assertEquals(list, answer);
    }

    @Test
    void test3() {
        ListNode list = createCycledList(new int[]{1}, -1);
        var answer = detectCycle(list);

        Assertions.assertNull(answer);
    }
}