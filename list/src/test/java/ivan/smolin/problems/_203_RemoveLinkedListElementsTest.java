package ivan.smolin.problems;

import ivan.smolin.core.AssertionUtils;
import org.junit.jupiter.api.Test;

import static ivan.smolin.core.ListUtils.createList;
import static ivan.smolin.problems._203_RemoveLinkedListElements.removeElements;

/**
 * LeetCode Problem 203. Remove Linked List Elements: https://leetcode.com/problems/remove-linked-list-elements/
 */
public final class _203_RemoveLinkedListElementsTest {
    @Test
    void test1() {
        var list = createList(new int[]{1, 2, 6, 3, 4, 5, 6});
        var answer = removeElements(list, 6);

        AssertionUtils.assertion(createList(new int[]{1, 2, 3, 4, 5}), answer);
    }

    @Test
    void test2() {
        var list = createList(new int[]{});
        var answer = removeElements(list, 1);

        AssertionUtils.assertion(createList(new int[]{}), answer);
    }

    @Test
    void test3() {
        var list = createList(new int[]{7, 7, 7, 7});
        var answer = removeElements(list, 7);

        AssertionUtils.assertion(createList(new int[]{}), answer);
    }
}