package ivan.smolin.problems;

import ivan.smolin.core.AssertionUtils;
import ivan.smolin.core.ListUtils.ListNode;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._206_ReverseLinkedList.reverseList;

/**
 * LeetCode Problem 206. Reverse Linked List: https://leetcode.com/problems/reverse-linked-list/
 */
public final class _206_ReverseLinkedListTest {
    @Test
    void test1() {
        var list = new ListNode(
            1,
            new ListNode(
                2,
                new ListNode(
                    3,
                    new ListNode(
                        4,
                        new ListNode(5)
                    )
                )
            )
        );

        var answer = reverseList(list);

        var expected = new ListNode(
            5,
            new ListNode(
                4,
                new ListNode(
                    3,
                    new ListNode(
                        2,
                        new ListNode(1)
                    )
                )
            )
        );

        AssertionUtils.assertion(expected, answer);
    }

    @Test
    void test2() {
        var list = new ListNode(1);

        var answer = reverseList(list);

        AssertionUtils.assertion(new ListNode(1), answer);
    }

    @Test
    void test3() {
        var list = new ListNode(1, new ListNode(3));

        var answer = reverseList(list);

        AssertionUtils.assertion(new ListNode(3, new ListNode(1)), answer);
    }

    @Test
    void test4() {
        var list = new ListNode();

        var answer = reverseList(list);

        AssertionUtils.assertion(new ListNode(), answer);
    }
}