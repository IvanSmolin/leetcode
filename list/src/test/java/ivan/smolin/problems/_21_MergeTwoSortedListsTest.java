package ivan.smolin.problems;

import ivan.smolin.core.AssertionUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.core.ListUtils.createList;
import static ivan.smolin.problems._21_MergeTwoSortedLists.mergeTwoLists;

/**
 * LeetCode Problem 21. Merge Two Sorted Lists: https://leetcode.com/problems/merge-two-sorted-lists/
 */
public final class _21_MergeTwoSortedListsTest {
    @Test
    void test1() {
        var answer = mergeTwoLists(createList(new int[]{1, 2, 4}), createList(new int[]{1, 3, 4}));
        var expected = new int[]{1, 1, 2, 3, 4, 4};

        AssertionUtils.assertion(expected, answer);
    }

    @Test
    void test2() {
        var answer = mergeTwoLists(createList(new int[]{2}), createList(new int[]{1}));
        var expected = new int[]{1, 2};

        AssertionUtils.assertion(expected, answer);
    }

    @Test
    void test3() {
        var answer = mergeTwoLists(null, null);

        Assertions.assertNull(answer);
    }

    @Test
    void test4() {
        var answer = mergeTwoLists(createList(new int[]{2}), null);
        var expected = new int[]{2};

        AssertionUtils.assertion(expected, answer);
    }

    @Test
    void test5() {
        var answer = mergeTwoLists(null, createList(new int[]{23}));
        var expected = new int[]{23};

        AssertionUtils.assertion(expected, answer);
    }
}