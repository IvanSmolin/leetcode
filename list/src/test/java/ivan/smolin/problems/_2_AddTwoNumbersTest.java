package ivan.smolin.problems;

import ivan.smolin.core.AssertionUtils;
import org.junit.jupiter.api.Test;

import static ivan.smolin.core.ListUtils.createList;
import static ivan.smolin.problems._2_AddTwoNumbers.addTwoNumbers;

/**
 * LeetCode Problem 2. Add Two Numbers: https://leetcode.com/problems/add-two-numbers/
 */
public final class _2_AddTwoNumbersTest {
    @Test
    void test1() {
        var answer = addTwoNumbers(
            createList(new int[]{2, 4, 3}),
            createList(new int[]{5, 6, 4})
        );

        AssertionUtils.assertion(answer, createList(new int[]{7, 0, 8}));
    }

    @Test
    void test2() {
        var answer = addTwoNumbers(
            createList(new int[]{0}),
            createList(new int[]{0})
        );

        AssertionUtils.assertion(answer, createList(new int[]{0}));
    }

    @Test
    void test3() {
        var answer = addTwoNumbers(
            createList(new int[]{9, 9, 9, 9, 9, 9, 9}),
            createList(new int[]{9, 9, 9, 9})
        );

        AssertionUtils.assertion(answer, createList(new int[]{8, 9, 9, 9, 0, 0, 0, 1}));
    }
}