package ivan.smolin.problems;

import ivan.smolin.core.AssertionUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.core.ListUtils.createList;
import static ivan.smolin.problems._83_RemoveDuplicatesFromSortedList.deleteDuplicates;

/**
 * LeetCode Problem 83. Remove Duplicates from Sorted List:
 * https://leetcode.com/problems/remove-duplicates-from-sorted-list/
 */
public final class _83_RemoveDuplicatesFromSortedListTest {
    @Test
    void test1() {
        var answer = deleteDuplicates(createList(new int[]{1, 1, 2}));

        AssertionUtils.assertion(createList(new int[]{1, 2}), answer);
    }

    @Test
    void test2() {
        var answer = deleteDuplicates(createList(new int[]{1, 1, 2, 3, 3}));

        AssertionUtils.assertion(createList(new int[]{1, 2, 3}), answer);
    }

    @Test
    void test3() {
        var answer = deleteDuplicates(createList(new int[]{}));

        Assertions.assertNull(answer);
    }
}