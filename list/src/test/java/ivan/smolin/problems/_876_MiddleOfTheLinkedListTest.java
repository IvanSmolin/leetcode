package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static ivan.smolin.core.ListUtils.createList;
import static ivan.smolin.problems._876_MiddleOfTheLinkedList.middleNode;

/**
 * LeetCode Problem 876. Middle of the Linked List: https://leetcode.com/problems/middle-of-the-linked-list/
 */
public final class _876_MiddleOfTheLinkedListTest {
    @Test
    void test1() {
        var answer = middleNode(createList(10));

        Assertions.assertEquals(Arrays.asList(6, 7, 8, 9, 10), answer.getLast());
    }

    @Test
    void test2() {
        var answer = middleNode(createList(9));

        Assertions.assertEquals(Arrays.asList(5, 6, 7, 8, 9), answer.getLast());
    }

    @Test
    void test3() {
        var answer = middleNode(createList(5));

        Assertions.assertEquals(Arrays.asList(3, 4, 5), answer.getLast());
    }

    @Test
    void test4() {
        var answer = middleNode(createList(6));

        Assertions.assertEquals(Arrays.asList(4, 5, 6), answer.getLast());
    }
}