package ivan.smolin.problems;

import ivan.smolin.core.AssertionUtils;
import ivan.smolin.core.ListUtils.ListNode;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._92_ReverseLinkedList2.reverseBetween;

/**
 * LeetCode Problem 92. Reverse Linked List II: https://leetcode.com/problems/reverse-linked-list-ii/
 */
public final class _92_ReverseLinkedList2Test {
    @Test
    void test1() {
        var list = new ListNode(
            1,
            new ListNode(
                2,
                new ListNode(
                    3,
                    new ListNode(
                        4,
                        new ListNode(5)
                    )
                )
            )
        );

        var expected = new ListNode(
            1,
            new ListNode(
                4,
                new ListNode(
                    3,
                    new ListNode(
                        2,
                        new ListNode(5)
                    )
                )
            )
        );

        var answer = reverseBetween(list, 2, 4);

        AssertionUtils.assertion(answer, expected);
    }

    @Test
    void test2() {
        var list = new ListNode(1);

        var expected = new ListNode(1);

        var answer = reverseBetween(list, 1, 1);

        AssertionUtils.assertion(answer, expected);
    }

    @Test
    void test3() {
        var list = new ListNode(
            1,
            new ListNode(
                2,
                new ListNode(
                    3,
                    new ListNode(
                        4,
                        new ListNode(5)
                    )
                )
            )
        );

        var expected = new ListNode(
            1,
            new ListNode(
                2,
                new ListNode(
                    4,
                    new ListNode(
                        3,
                        new ListNode(5)
                    )
                )
            )
        );

        var answer = reverseBetween(list, 3, 4);

        AssertionUtils.assertion(answer, expected);
    }

    @Test
    void test4() {
        var list = new ListNode(
            3,
            new ListNode(5)
        );

        var expected = new ListNode(
            5,
            new ListNode(3)
        );

        var answer = reverseBetween(list, 1, 2);

        AssertionUtils.assertion(answer, expected);
    }
}