package ivan.smolin.problems;

import java.util.Arrays;
import java.util.List;

/**
 * LeetCode Problem 175. Combine Two Tables: https://leetcode.com/problems/combine-two-tables/
 */
public final class _175_CombineTwoTables {
    public static List<String> combineTwoTables() {
        return Arrays.asList(
            "select p.firstName, p.lastName, a.city, a.state",
            "from Person as p",
            "left join Address as a on p.personId = a.personId"
        );
    }
}