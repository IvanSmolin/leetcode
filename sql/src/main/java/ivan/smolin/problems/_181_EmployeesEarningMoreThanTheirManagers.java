package ivan.smolin.problems;

import java.util.Arrays;
import java.util.List;

/**
 * LeetCode Problem 181. Employees Earning More Than Their Managers:
 * https://leetcode.com/problems/employees-earning-more-than-their-managers/
 */
public final class _181_EmployeesEarningMoreThanTheirManagers {
    public static List<String> employeesEarningMoreThanTheirManagers() {
        return Arrays.asList(
            "select e.name as 'Employee'",
            "from Employee as e, Employee m",
            "where m.id = e.managerId and e.salary > m.salary"
        );
    }
}