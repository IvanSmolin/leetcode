package ivan.smolin.problems;

import java.util.Arrays;
import java.util.List;

/**
 * LeetCode Problem 182. Duplicate Emails: https://leetcode.com/problems/duplicate-emails/
 */
public final class _182_DuplicateEmails {
    public static List<String> duplicateEmails() {
        return Arrays.asList(
            "select email as 'Email'",
            "from Person ",
            "group by email",
            "having count(email) > 1"
        );
    }
}