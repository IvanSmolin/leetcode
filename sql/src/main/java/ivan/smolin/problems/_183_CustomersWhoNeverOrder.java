package ivan.smolin.problems;

import java.util.Arrays;
import java.util.List;

/**
 * LeetCode Problem 183. Customers Who Never Order: https://leetcode.com/problems/customers-who-never-order/
 */
public final class _183_CustomersWhoNeverOrder {
    public static List<String> customersWhoNeverOrder() {
        return Arrays.asList(
            "select c.name as 'Customers'",
            "from Customers as c",
            "left join Orders as o on c.id = o.customerId",
            "where o.id is null"
        );
    }
}