package ivan.smolin.problems;

import java.util.Arrays;
import java.util.List;

/**
 * LeetCode Problem 196. Delete Duplicate Emails: https://leetcode.com/problems/delete-duplicate-emails/
 */
public final class _196_DeleteDuplicateEmails {
    public static List<String> deleteDuplicateEmails() {
        return Arrays.asList(
            "delete p1 ",
            "from Person as p1",
            "join (",
            "    select min(id) as min, email",
            "    from Person",
            "    group by email",
            "    having count(email) > 1",
            ") p2 on p2.email = p1.email and min != p1.id"
        );
    }
}