package ivan.smolin.problems;

import java.util.Arrays;
import java.util.List;

/**
 * LeetCode Problem 197. Rising Temperature: https://leetcode.com/problems/rising-temperature/
 */
public final class _197_RisingTemperature {
    public static List<String> risingTemperature() {
        return Arrays.asList(
            "select w1.id",
            "from Weather as w1",
            "join Weather as w2 on w1.recordDate = date_add(w2.recordDate, interval 1 day)",
            "where w1.temperature > w2.temperature"
        );
    }
}