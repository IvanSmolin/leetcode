package ivan.smolin.problems;

import java.util.Arrays;
import java.util.List;

/**
 * LeetCode Problem 511. Game Play Analysis I: https://leetcode.com/problems/game-play-analysis-i/
 */
public final class _511_GamePlayAnalysis1 {
    public static List<String> gamePlayAnalysis1() {
        return Arrays.asList(
            "select player_id, min(event_date) as 'first_login'",
            "from Activity",
            "group by player_id"
        );
    }
}