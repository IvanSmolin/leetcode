package ivan.smolin.problems;

import java.util.Arrays;
import java.util.List;

/**
 * LeetCode Problem 577. Employee Bonus: https://leetcode.com/problems/employee-bonus/
 */
public final class _577_EmployeeBonus {
    public static List<String> employeeBonus() {
        return Arrays.asList(
            "select e.name, b.bonus",
            "from Employee as e",
            "left join Bonus as b on e.empId = b.empId",
            "where b.bonus < 1000 or b.bonus is null"
        );
    }
}