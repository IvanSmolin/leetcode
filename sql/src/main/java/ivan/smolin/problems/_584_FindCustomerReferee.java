package ivan.smolin.problems;

import java.util.Arrays;
import java.util.List;

/**
 * LeetCode Problem 584. Find Customer Referee: https://leetcode.com/problems/find-customer-referee/
 */
public final class _584_FindCustomerReferee {
    public static List<String> findCustomerReferee() {
        return Arrays.asList(
            "select name",
            "from Customer",
            "where referee_id != 2 or referee_id is null"
        );
    }
}