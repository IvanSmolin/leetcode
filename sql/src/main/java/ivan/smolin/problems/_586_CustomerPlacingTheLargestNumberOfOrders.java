package ivan.smolin.problems;

import java.util.Arrays;
import java.util.List;

/**
 * LeetCode Problem 586. Customer Placing the Largest Number of Orders:
 * https://leetcode.com/problems/customer-placing-the-largest-number-of-orders/
 */
public final class _586_CustomerPlacingTheLargestNumberOfOrders {
    public static List<String> customerPlacingTheLargestNumberOfOrders() {
        return Arrays.asList(
            "select customer_number",
            "from Orders",
            "group by customer_number",
            "order by count(order_number) desc",
            "limit 1"
        );
    }
}