package ivan.smolin.problems;

import java.util.Arrays;
import java.util.List;

/**
 * LeetCode Problem 595. Big Countries: https://leetcode.com/problems/big-countries/
 */
public final class _595_BigCountries {
    public static List<String> bigCountries() {
        return Arrays.asList(
            "select name, population, area",
            "from World",
            "where area >= 3000000 or population >= 25000000"
        );
    }
}