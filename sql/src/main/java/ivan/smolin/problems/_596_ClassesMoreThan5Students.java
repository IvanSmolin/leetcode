package ivan.smolin.problems;

import java.util.Arrays;
import java.util.List;

/**
 * LeetCode Problem 596. Classes More Than 5 Students: https://leetcode.com/problems/classes-more-than-5-students/
 */
public final class _596_ClassesMoreThan5Students {
    public static List<String> classesMoreThan5Students() {
        return Arrays.asList(
            "select class",
            "from Courses",
            "group by class",
            "having count(student) >= 5"
        );
    }
}