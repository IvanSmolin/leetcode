package ivan.smolin.problems;

/**
 * LeetCode Problem 125. Valid Palindrome: https://leetcode.com/problems/valid-palindrome/
 */
public final class _125_ValidPalindrome {
    public static boolean isPalindrome(String s) {
        s = s.toLowerCase().replaceAll("[^a-z0-9]", "");

        var i = 0;
        var j = s.length() - 1;

        while (i < j) {
            if (s.charAt(i) != s.charAt(j)) {
                return false;
            }

            i++;
            j--;
        }

        return true;
    }
}