package ivan.smolin.problems;

/**
 * LeetCode Problem 171. Excel Sheet Column Number: https://leetcode.com/problems/excel-sheet-column-number/
 */
public final class _171_ExcelSheetColumnNumber {
    public static int titleToNumber(String columnTitle) {
        var result = 0;

        for (var c : columnTitle.toCharArray()) {
            result = result * 26 + (c - 'A' + 1);
        }

        return result;
    }
}