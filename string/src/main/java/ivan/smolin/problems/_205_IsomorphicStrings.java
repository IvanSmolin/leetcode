package ivan.smolin.problems;

import java.util.HashMap;

/**
 * LeetCode Problem 205. Isomorphic Strings: https://leetcode.com/problems/isomorphic-strings/
 */
public final class _205_IsomorphicStrings {
    // TIPS: s.length() and s.charAt(i) important for performance

    /**
     * First implementation
     */
    public static boolean isIsomorphic1(String s, String t) {
        var length = s.length();

        if (length != t.length()) {
            return false;
        }

        var mapS = new HashMap<Character, Character>();
        var mapT = new HashMap<Character, Boolean>();

        for (int i = 0; i < length; i++) {
            var charS = s.charAt(i);
            var charT = t.charAt(i);

            if (!mapS.containsKey(charS)) {
                if (mapT.containsKey(charT)) {
                    return false;
                }

                mapS.put(charS, charT);
                mapT.put(charT, true);
            } else if (mapS.get(charS) != charT) {
                return false;
            }
        }

        return true;
    }

    /**
     * Second implementation
     */
    public static boolean isIsomorphic(String s, String t) {
        var length = s.length();

        if (length != t.length()) {
            return false;
        }

        var mapS = new int[127];
        var mapT = new int[127];

        for (var i = 0; i < length; i++) {
            var charS = s.charAt(i);
            var charT = t.charAt(i);

            if (mapS[charS] != mapT[charT]) {
                return false;
            }

            mapS[charS] = i + 1;
            mapT[charT] = i + 1;
        }

        return true;
    }
}