package ivan.smolin.problems;

import java.util.Stack;

/**
 * LeetCode Problem 20. Valid Parentheses: https://leetcode.com/problems/valid-parentheses/
 */
public final class _20_ValidParentheses {
    public static boolean isValid(String s) {
        var stack = new Stack<Character>();

        for (char c : s.toCharArray()) {
            if (c == '(' || c == '{' || c == '[') {
                stack.push(c);
            } else if (
                !stack.isEmpty() &&
                    (c == ')' && stack.peek() == '(' ||
                        c == '}' && stack.peek() == '{' ||
                        c == ']' && stack.peek() == '[')
            ) {
                stack.pop();
            } else {
                return false;
            }
        }

        return stack.isEmpty();
    }
}