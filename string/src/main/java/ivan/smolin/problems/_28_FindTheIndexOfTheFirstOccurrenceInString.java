package ivan.smolin.problems;

/**
 * LeetCode Problem 28. Find the Index of the First Occurrence in a String:
 * https://leetcode.com/problems/find-the-index-of-the-first-occurrence-in-a-string/
 */
public final class _28_FindTheIndexOfTheFirstOccurrenceInString {
    public static int strStr(String haystack, String needle) {
        for (int i = 0; i <= haystack.length() - needle.length(); i++) {
            if (needle.equals(haystack.substring(i, i + needle.length()))) {
                return i;
            }
        }

        return -1;
    }
}