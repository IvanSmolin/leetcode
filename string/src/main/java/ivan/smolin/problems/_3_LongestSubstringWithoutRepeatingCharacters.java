package ivan.smolin.problems;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * LeetCode Problem 3. Longest Substring Without Repeating Characters:
 * https://leetcode.com/problems/longest-substring-without-repeating-characters/
 */
public final class _3_LongestSubstringWithoutRepeatingCharacters {
    /**
     * Version 3
     */
    public static int lengthOfLongestSubstring(String s) {
        int start = 0;
        int end = 0;
        int maxLength = 0;
        Set<Character> uniqueCharacters = new HashSet<>();

        while (end < s.length()) {
            if (uniqueCharacters.add(s.charAt(end))) {
                end++;
                maxLength = Math.max(maxLength, uniqueCharacters.size());
            } else {
                uniqueCharacters.remove(s.charAt(start));
                start++;
            }
        }

        return maxLength;
    }

    /**
     * Version 2
     */
    public static int lengthOfLongestSubstring2(String s) {
        int result = 0;
        int[] lastIndex = new int[127];
        Arrays.fill(lastIndex, -1);
        int index = 0;

        for (int i = 0; i < s.length(); i++) {
            index = Math.max(index, lastIndex[s.charAt(i)] + 1);
            result = Math.max(result, i - index + 1);
            lastIndex[s.charAt(i)] = i;
        }

        return result;
    }

    /**
     * Version 1 - with using HashMap
     */
    public static int lengthOfLongestSubstring1(String s) {
        Map<Character, Integer> buffer = new HashMap<>();
        char[] string = s.toCharArray();
        int max = 0;
        int current = 0;
        int forbiddenIndex = 0;
        int index = 0;

        for (char c : string) {
            if (buffer.containsKey(c) && buffer.get(c) >= forbiddenIndex) {
                max = Math.max(max, current);
                forbiddenIndex = buffer.get(c);
                current = index - forbiddenIndex;
                buffer.put(c, index);
            } else {
                buffer.put(c, index);
                current++;
            }

            index++;
        }

        return Math.max(max, current);
    }
}