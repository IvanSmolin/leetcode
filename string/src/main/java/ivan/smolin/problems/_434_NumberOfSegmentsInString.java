package ivan.smolin.problems;

/**
 * LeetCode Problem 434. Number of Segments in a String: https://leetcode.com/problems/number-of-segments-in-a-string/
 */
public final class _434_NumberOfSegmentsInString {
    public static int countSegments(String s) {
        boolean flag = false;
        byte result = 0;

        for (char c : s.toCharArray()) {
            if (flag && c == ' ') {
                flag = false;
            } else if (!flag && c != ' ') {
                flag = true;
                result++;
            }
        }

        return result;
    }
}