package ivan.smolin.problems;

/**
 * LeetCode Problem 58. Length of Last Word: https://leetcode.com/problems/length-of-last-word/
 */
public final class _58_LengthOfLastWord {
    public static int lengthOfLastWord(String s) {
        var result = 0;
        var length = s.length() - 1;

        while (length >= 0 && s.charAt(length) == ' ') {
            length--;
        }

        while (length >= 0 && s.charAt(length) != ' ') {
            result++;
            length--;
        }

        return result;
    }
}