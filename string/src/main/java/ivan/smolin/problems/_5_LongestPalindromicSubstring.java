package ivan.smolin.problems;

/**
 * LeetCode Problem 5. Longest Palindromic Substring: https://leetcode.com/problems/longest-palindromic-substring/
 */
public final class _5_LongestPalindromicSubstring {
    /**
     * Manacher’s Algorithm
     */
    public static String longestPalindrome1(String s) {
        int n = s.length();
        if (n < 2) {
            return s;
        }

        n = 2 * n + 1;              // Position count
        int[] l = new int[n + 1];   // LPS Length Array
        l[0] = 0;
        l[1] = 1;
        int c = 1;                  // centerPosition
        int r = 2;                  // centerRightPosition
        int i;                      // currentRightPosition
        int mirror;                 // currentLeftPosition
        int maxLpsLength = 0;
        int maxLpsCenterPosition = 0;
        int start;
        int end;
        int diff;

        // Uncomment it to print LPS Length array
        // System.out.println(l[0] + " " + l[1]);
        for (i = 2; i < n; i++) {
            // get currentLeftPosition mirror
            // for currentRightPosition i
            mirror = 2 * c - i;
            l[i] = 0;
            diff = r - i;

            // If currentRightPosition i is within
            // centerRightPosition r
            if (diff > 0) {
                l[i] = Math.min(l[mirror], diff);
            }

            // Attempt to expand palindrome centered at
            // currentRightPosition i. Here for odd positions,
            // we compare characters and if match then
            // increment LPS Length by ONE. If even position,
            // we just increment LPS by ONE without
            // any character comparison
            while (
                (i + l[i]) + 1 < n && (i - l[i]) > 0 &&
                    ((i + l[i] + 1) % 2 == 0 || s.charAt((i + l[i] + 1) / 2) == s.charAt((i - l[i] - 1) / 2))
            ) {
                l[i]++;
            }

            // Track maxLpsLength
            if (l[i] > maxLpsLength) {
                maxLpsLength = l[i];
                maxLpsCenterPosition = i;
            }

            // If palindrome centered at currentRightPosition i
            // expand beyond centerRightPosition r,
            // adjust centerPosition c based on expanded palindrome.
            if (i + l[i] > r) {
                c = i;
                r = i + l[i];
            }

            // Uncomment it to print LPS Length array
            // System.out.println(l[i]);
        }

        start = (maxLpsCenterPosition - maxLpsLength) / 2;
        end = start + maxLpsLength;

        return s.substring(start, end);
    }

    /**
     * Version 1
     */
    public static String longestPalindrome(String s) {
        int count = 1;
        String maxPalindrome = "";
        StringBuilder currentPalindrome = new StringBuilder();
        char[] str = s.toCharArray();

        for (int i = 0; i < str.length; i++) {
            currentPalindrome.append(str[i]);

            while (i - count >= 0 && i + count < s.length() && str[i - count] == str[i + count]) {
                currentPalindrome.insert(0, str[i - count]).append(str[i + count]);
                count++;
            }

            if (maxPalindrome.length() < currentPalindrome.length()) {
                maxPalindrome = currentPalindrome.toString();
            }

            currentPalindrome.setLength(0);
            count = 1;
        }

        for (int i = 0; i < str.length - 1; i++) {
            currentPalindrome.setLength(0);
            currentPalindrome.append(str[i]);

            if (str[i] == str[i + 1]) {
                currentPalindrome.append(str[i + 1]);

                while (
                    str[i] == str[i + 1] && i - count >= 0 && i + 1 + count < s.length() &&
                        str[i - count] == str[i + 1 + count]
                ) {
                    currentPalindrome.insert(0, str[i - count]).append(str[i + 1 + count]);
                    count++;
                }

                if (maxPalindrome.length() < currentPalindrome.length()) {
                    maxPalindrome = currentPalindrome.toString();
                }

                count = 1;
            }
        }

        return maxPalindrome;
    }
}