package ivan.smolin.problems;

/**
 * LeetCode Problem 67. Add Binary: https://leetcode.com/problems/add-binary/
 */
public final class _67_AddBinary {
    public static String addBinary(String a, String b) {
        var result = new StringBuilder();
        var i = a.length() - 1;
        var j = b.length() - 1;
        var sum = 0;

        while (i >= 0 || j >= 0) {
            if (i >= 0) {
                sum += a.charAt(i--) - '0';
            }

            if (j >= 0) {
                sum += b.charAt(j--) - '0';
            }

            result.append(sum % 2);
            sum /= 2;
        }

        if (sum != 0) {
            result.append(sum);
        }

        return result.reverse().toString();
    }
}