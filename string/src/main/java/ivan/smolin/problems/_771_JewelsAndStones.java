package ivan.smolin.problems;

/**
 * LeetCode Problem 771. Jewels and Stones: https://leetcode.com/problems/jewels-and-stones/
 */
public final class _771_JewelsAndStones {
    public static int numJewelsInStones(String jewels, String stones) {
        byte result = 0;

        for (char c : stones.toCharArray()) {
            if (jewels.contains(String.valueOf(c))) {
                result++;
            }
        }

        return result;
    }
}