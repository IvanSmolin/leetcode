package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._125_ValidPalindrome.isPalindrome;

/**
 * LeetCode Problem 125. Valid Palindrome: https://leetcode.com/problems/valid-palindrome/
 */
public final class _125_ValidPalindromeTest {
    @Test
    void test1() {
        var actual = isPalindrome("A man, a plan, a canal: Panama");

        Assertions.assertTrue(actual);
    }

    @Test
    void test2() {
        var actual = isPalindrome("race a car");

        Assertions.assertFalse(actual);
    }

    @Test
    void test3() {
        var actual = isPalindrome(" ");

        Assertions.assertTrue(actual);
    }

    @Test
    void test4() {
        var actual = isPalindrome("A man, a plane, a canal: Panama");

        Assertions.assertFalse(actual);
    }
}