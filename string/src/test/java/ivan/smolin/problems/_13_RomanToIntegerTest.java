package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._13_RomanToInteger.romanToInt;

/**
 * LeetCode Problem 13. Roman to Integer: https://leetcode.com/problems/roman-to-integer/
 */
public final class _13_RomanToIntegerTest {
    @Test
    void test1() {
        var answer = romanToInt("XIV");

        Assertions.assertEquals(14, answer);
    }

    @Test
    void test2() {
        var answer = romanToInt("III");

        Assertions.assertEquals(3, answer);
    }

    @Test
    void test3() {
        var answer = romanToInt("LVIII");

        Assertions.assertEquals(58, answer);
    }

    @Test
    void test4() {
        var answer = romanToInt("MCMXCIV");

        Assertions.assertEquals(1994, answer);
    }
}