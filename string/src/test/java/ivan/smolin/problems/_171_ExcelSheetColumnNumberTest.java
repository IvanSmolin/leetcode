package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._171_ExcelSheetColumnNumber.titleToNumber;

/**
 * LeetCode Problem 171. Excel Sheet Column Number: https://leetcode.com/problems/excel-sheet-column-number/
 */
public final class _171_ExcelSheetColumnNumberTest {
    @Test
    void test1() {
        var answer = titleToNumber("A");

        Assertions.assertEquals(1, answer);
    }

    @Test
    void test2() {
        var answer = titleToNumber("AB");

        Assertions.assertEquals(28, answer);
    }

    @Test
    void test3() {
        var answer = titleToNumber("ZY");

        Assertions.assertEquals(701, answer);
    }

    @Test
    void test4() {
        var answer = titleToNumber("EDREC");

        Assertions.assertEquals(2367485, answer);
    }
}