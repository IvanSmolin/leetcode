package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._205_IsomorphicStrings.isIsomorphic;

/**
 * LeetCode Problem 205. Isomorphic Strings: https://leetcode.com/problems/isomorphic-strings/
 */
public final class _205_IsomorphicStringsTest {
    @Test
    void test1() {
        var answer = isIsomorphic("egg", "add");

        Assertions.assertTrue(answer);
    }

    @Test
    void test2() {
        var answer = isIsomorphic("foo", "bar");

        Assertions.assertFalse(answer);
    }

    @Test
    void test3() {
        var answer = isIsomorphic("paper", "title");

        Assertions.assertTrue(answer);
    }

    @Test
    void test4() {
        var answer = isIsomorphic("badc", "baba");

        Assertions.assertFalse(answer);
    }

    @Test
    void test5() {
        var answer = isIsomorphic("badc", "aaaa");

        Assertions.assertFalse(answer);
    }

    @Test
    void test6() {
        var answer = isIsomorphic("bxdc", "aaaa");

        Assertions.assertFalse(answer);
    }

    @Test
    void test7() {
        var answer = isIsomorphic("ab", "aa");

        Assertions.assertFalse(answer);
    }

    @Test
    void test8() {
        var answer = isIsomorphic("bbbaaaba", "aaabbbba");

        Assertions.assertFalse(answer);
    }
}