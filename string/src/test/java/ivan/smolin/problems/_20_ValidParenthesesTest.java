package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._20_ValidParentheses.isValid;

/**
 * LeetCode Problem 20. Valid Parentheses: https://leetcode.com/problems/valid-parentheses/
 */
public final class _20_ValidParenthesesTest {
    @Test
    void test1() {
        var answer = isValid("()[]{}");

        Assertions.assertTrue(answer);
    }

    @Test
    void test2() {
        var answer = isValid("()");

        Assertions.assertTrue(answer);
    }

    @Test
    void test3() {
        var answer = isValid("(]");

        Assertions.assertFalse(answer);
    }

    @Test
    void test4() {
        var answer = isValid("({[][]})");

        Assertions.assertTrue(answer);
    }

    @Test
    void test5() {
        var answer = isValid("({[][{}]})");

        Assertions.assertTrue(answer);
    }

    @Test
    void test6() {
        var answer = isValid("({[[}][{}]})");

        Assertions.assertFalse(answer);
    }

    @Test
    void test7() {
        var answer = isValid(")(");

        Assertions.assertFalse(answer);
    }

    @Test
    void test8() {
        var answer = isValid("]");

        Assertions.assertFalse(answer);
    }
}