package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._28_FindTheIndexOfTheFirstOccurrenceInString.strStr;

/**
 * LeetCode Problem 28. Find the Index of the First Occurrence in a String:
 * https://leetcode.com/problems/find-the-index-of-the-first-occurrence-in-a-string/
 */
public final class _28_FindTheIndexOfTheFirstOccurrenceInStringTest {
    @Test
    void test1() {
        var answer = strStr("sadbutsad", "sad");

        Assertions.assertEquals(0, answer);
    }

    @Test
    void test2() {
        var answer = strStr("leetcode", "leeto");

        Assertions.assertEquals(-1, answer);
    }

    @Test
    void test3() {
        var answer = strStr("a", "a");

        Assertions.assertEquals(0, answer);
    }

    @Test
    void test4() {
        var answer = strStr("abc", "a");

        Assertions.assertEquals(0, answer);
    }

    @Test
    void test5() {
        var answer = strStr("abc", "b");

        Assertions.assertEquals(1, answer);
    }

    @Test
    void test6() {
        var answer = strStr("abc", "c");

        Assertions.assertEquals(2, answer);
    }
}