package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._3_LongestSubstringWithoutRepeatingCharacters.lengthOfLongestSubstring;

/**
 * LeetCode Problem 3. Longest Substring Without Repeating Characters:
 * https://leetcode.com/problems/longest-substring-without-repeating-characters/
 */
public final class _3_LongestSubstringWithoutRepeatingCharactersTest {
    @Test
    void test1() {
        var answer = lengthOfLongestSubstring("abcabcbb");

        Assertions.assertEquals(3, answer);
    }

    @Test
    void test2() {
        var answer = lengthOfLongestSubstring("bbbbb");

        Assertions.assertEquals(1, answer);
    }

    @Test
    void test3() {
        var answer = lengthOfLongestSubstring("pwwkew");

        Assertions.assertEquals(3, answer);
    }

    @Test
    void test4() {
        var answer = lengthOfLongestSubstring(" ");

        Assertions.assertEquals(1, answer);
    }

    @Test
    void test5() {
        var answer = lengthOfLongestSubstring("Ivan Smolin");

        Assertions.assertEquals(10, answer);
    }

    @Test
    void test6() {
        var answer = lengthOfLongestSubstring("tmmzuxt");

        Assertions.assertEquals(5, answer);
    }
}