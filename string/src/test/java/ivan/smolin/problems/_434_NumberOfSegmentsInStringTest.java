package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._434_NumberOfSegmentsInString.countSegments;

/**
 * LeetCode Problem 434. Number of Segments in a String: https://leetcode.com/problems/number-of-segments-in-a-string/
 */
public final class _434_NumberOfSegmentsInStringTest {
    @Test
    void test1() {
        var answer = countSegments("Hello, my name is John");

        Assertions.assertEquals(5, answer);
    }

    @Test
    void test2() {
        var answer = countSegments("Hello");

        Assertions.assertEquals(1, answer);
    }

    @Test
    void test3() {
        var answer = countSegments(" ");

        Assertions.assertEquals(0, answer);
    }
}