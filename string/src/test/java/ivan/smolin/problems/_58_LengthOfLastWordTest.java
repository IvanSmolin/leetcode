package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._58_LengthOfLastWord.lengthOfLastWord;

/**
 * LeetCode Problem 58. Length of Last Word: https://leetcode.com/problems/length-of-last-word/
 */
public final class _58_LengthOfLastWordTest {
    @Test
    void test1() {
        var answer = lengthOfLastWord("Hello World");

        Assertions.assertEquals(5, answer);
    }

    @Test
    void test2() {
        var answer = lengthOfLastWord("   fly me   to   the moon  ");

        Assertions.assertEquals(4, answer);
    }

    @Test
    void test3() {
        var answer = lengthOfLastWord("luffy is still joyboy");

        Assertions.assertEquals(6, answer);
    }

    @Test
    void test4() {
        var answer = lengthOfLastWord("     luffyisstilljoyboy      ");

        Assertions.assertEquals(18, answer);
    }

    @Test
    void test5() {
        var answer = lengthOfLastWord("luffyisstilljoyboy");

        Assertions.assertEquals(18, answer);
    }
}