package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._5_LongestPalindromicSubstring.longestPalindrome;

/**
 * LeetCode Problem 5. Longest Palindromic Substring: https://leetcode.com/problems/longest-palindromic-substring/
 */
public final class _5_LongestPalindromicSubstringTest {
    @Test
    void test1() {
        String answer = longestPalindrome("babad");

        Assertions.assertEquals("bab", answer);
    }

    @Test
    void test2() {
        String answer = longestPalindrome("cbbd");

        Assertions.assertEquals("bb", answer);
    }

    @Test
    void test3() {
        String answer = longestPalindrome("babcbabcbaccba");

        Assertions.assertEquals("abcbabcba", answer);
    }

    @Test
    void test4() {
        String answer = longestPalindrome("abaaba");

        Assertions.assertEquals("abaaba", answer);
    }

    @Test
    void test5() {
        String answer = longestPalindrome("abababa");

        Assertions.assertEquals("abababa", answer);
    }

    @Test
    void test6() {
        String answer = longestPalindrome("abcbabcbabcba");

        Assertions.assertEquals("abcbabcbabcba", answer);
    }

    @Test
    void test7() {
        String answer = longestPalindrome("forgeeksskeegfor");

        Assertions.assertEquals("geeksskeeg", answer);
    }

    @Test
    void test8() {
        String answer = longestPalindrome("caba");

        Assertions.assertEquals("aba", answer);
    }

    @Test
    void test9() {
        String answer = longestPalindrome("abacdfgdcaba");

        Assertions.assertEquals("aba", answer);
    }

    @Test
    void test10() {
        String answer = longestPalindrome("abacdfgdcabba");

        Assertions.assertEquals("abba", answer);
    }

    @Test
    void test11() {
        String answer = longestPalindrome("abacdedcaba");

        Assertions.assertEquals("abacdedcaba", answer);
    }

    @Test
    void test12() {
        String answer = longestPalindrome("f");

        Assertions.assertEquals("f", answer);
    }

    @Test
    void test13() {
        String answer = longestPalindrome("af");

        Assertions.assertEquals("a", answer);
    }

    @Test
    void test14() {
        String answer = longestPalindrome("qwertyuiop");

        Assertions.assertEquals("q", answer);
    }

    @Test
    void test15() {
        String answer = longestPalindrome("q");

        Assertions.assertEquals("q", answer);
    }
}