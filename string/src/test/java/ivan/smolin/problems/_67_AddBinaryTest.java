package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._67_AddBinary.addBinary;

/**
 * LeetCode Problem 67. Add Binary: https://leetcode.com/problems/add-binary/
 */
public final class _67_AddBinaryTest {
    @Test
    void test1() {
        var answer = addBinary("11", "1");

        Assertions.assertEquals("100", answer);
    }

    @Test
    void test2() {
        var answer = addBinary("1010", "1011");

        Assertions.assertEquals("10101", answer);
    }
}