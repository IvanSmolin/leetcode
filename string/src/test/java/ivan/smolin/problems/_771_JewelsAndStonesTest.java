package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._771_JewelsAndStones.numJewelsInStones;

/**
 * LeetCode Problem 771. Jewels and Stones: https://leetcode.com/problems/jewels-and-stones/
 */
public final class _771_JewelsAndStonesTest {
    @Test
    void test1() {
        var answer = numJewelsInStones("aA", "aAAbbbb");

        Assertions.assertEquals(3, answer);
    }

    @Test
    void test2() {
        var answer = numJewelsInStones("z", "ZZ");

        Assertions.assertEquals(0, answer);
    }
}