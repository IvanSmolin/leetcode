package ivan.smolin.problems;

import java.util.LinkedList;
import java.util.Queue;

/**
 * LeetCode Problem 225. Implement Stack using Queues: https://leetcode.com/problems/add-two-numbers/
 */
public final class _225_ImplementStackUsingQueues {
    public static class MyStack {
        private Queue<Integer> queue1;

        private Queue<Integer> queue2;

        private int last;

        public MyStack() {
            queue1 = new LinkedList<>();
            queue2 = new LinkedList<>();
        }

        public void push(int x) {
            queue1.offer(x);
            last = x;
        }

        public int pop() {
            while (queue1.size() > 1) {
                last = queue1.remove();
                queue2.offer(last);
            }

            var deleted = queue1.remove();
            queue1 = queue2;
            queue2 = new LinkedList<>();

            return deleted;
        }

        public int top() {
            return last;
        }

        public boolean empty() {
            return queue1.isEmpty();
        }
    }
}