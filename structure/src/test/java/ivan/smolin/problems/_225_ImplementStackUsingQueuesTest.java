package ivan.smolin.problems;

import ivan.smolin.problems._225_ImplementStackUsingQueues.MyStack;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * LeetCode Problem 225. Implement Stack using Queues: https://leetcode.com/problems/add-two-numbers/
 */
public final class _225_ImplementStackUsingQueuesTest {
    @Test
    void test1() {
        MyStack myStack = new MyStack();
        myStack.push(1);
        myStack.push(2);
        Assertions.assertEquals(2, myStack.top());
        Assertions.assertEquals(2, myStack.pop());
        Assertions.assertFalse(myStack.empty());
    }
}