package ivan.smolin.core;

import ivan.smolin.core.TreeUtils.TreeNode;
import org.junit.jupiter.api.Assertions;

public final class AssertionUtils {
    public static void compareTrees(TreeNode expected, TreeNode actual) {
        // Check if both trees are null
        if (expected == null && actual == null) {
            return;
        }

        // Check if either tree is null
        if (expected == null || actual == null) {
            Assertions.fail("Expected tree and actual tree are different");
        }

        // Check if the values of the nodes are equal
        Assertions.assertEquals(expected.val, actual.val, "Values of expected and actual nodes are different");

        // Recursively compare the left and right subtrees
        compareTrees(expected.left, actual.left);
        compareTrees(expected.right, actual.right);
    }
}