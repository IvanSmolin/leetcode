package ivan.smolin.core;

import java.util.LinkedList;

public final class TreeUtils {
    public static void printTree(TreeNode root) {
        var queue = new LinkedList<TreeNode>();
        queue.add(root);

        while (!queue.isEmpty()) {
            var levelSize = queue.size();

            for (int i = 0; i < levelSize; i++) {
                var node = queue.poll();

                System.out.print(node.val + " ");

                if (node.left != null) {
                    queue.add(node.left);
                }

                if (node.right != null) {
                    queue.add(node.right);
                }
            }

            System.out.println();
        }
    }

    public static TreeNode buildTreeFromArray(Integer[] arr) {
        if (arr == null || arr.length == 0 || arr[0] == null) {
            return null;
        }

        var queue = new LinkedList<TreeNode>();
        var root = new TreeNode(arr[0]);
        queue.offer(root);

        int i = 1;
        while (i < arr.length) {
            var parent = queue.poll();

            if (arr[i] != null) {
                parent.left = new TreeNode(arr[i]);
                queue.offer(parent.left);
            }
            i++;

            if (i < arr.length && arr[i] != null) {
                parent.right = new TreeNode(arr[i]);
                queue.offer(parent.right);
            }
            i++;
        }

        return root;
    }

    public static class TreeNode {
        public int val;

        public TreeNode left;

        public TreeNode right;

        public TreeNode() {
        }

        public TreeNode(int val) {
            this.val = val;
        }

        public TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
}