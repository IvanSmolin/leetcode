package ivan.smolin.problems;

import ivan.smolin.core.TreeUtils.TreeNode;

/**
 * LeetCode Problem 100. Same Tree: https://leetcode.com/problems/same-tree/
 */
public final class _100_SameTree {
    public static boolean isSameTree(TreeNode p, TreeNode q) {
        if (p == null || q == null) {
            return p == q;
        }

        if (p.val != q.val) {
            return false;
        }

        return isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
    }
}