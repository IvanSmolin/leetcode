package ivan.smolin.problems;

import ivan.smolin.core.TreeUtils.TreeNode;

/**
 * LeetCode Problem 101. Symmetric Tree: https://leetcode.com/problems/symmetric-tree/
 */
public final class _101_SymmetricTree {
    public static boolean isSymmetric(TreeNode root) {
        return isSymmetricTree(root.left, root.right);
    }

    private static boolean isSymmetricTree(TreeNode root1, TreeNode root2) {
        if (root1 == null || root2 == null) {
            return root1 == root2;
        }

        if (root1.val != root2.val) {
            return false;
        }

        return isSymmetricTree(root1.left, root2.right) && isSymmetricTree(root1.right, root2.left);
    }
}