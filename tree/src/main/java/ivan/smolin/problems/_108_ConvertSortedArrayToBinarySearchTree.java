package ivan.smolin.problems;

import ivan.smolin.core.TreeUtils.TreeNode;

/**
 * LeetCode Problem 108. Convert Sorted Array to Binary Search Tree:
 * https://leetcode.com/problems/convert-sorted-array-to-binary-search-tree/
 */
public final class _108_ConvertSortedArrayToBinarySearchTree {
    public static TreeNode sortedArrayToBst(int[] nums) {
        return constructTree(nums, 0, nums.length - 1);
    }

    private static TreeNode constructTree(int[] nums, int left, int right) {
        if (left > right) {
            return null;
        }

        int mid = left + (right - left) / 2;
        TreeNode l = constructTree(nums, left, mid - 1);
        TreeNode r = constructTree(nums, mid + 1, right);
        return new TreeNode(nums[mid], l, r);
    }
}