package ivan.smolin.problems;

import ivan.smolin.core.TreeUtils.TreeNode;

/**
 * LeetCode Problem 110. Balanced Binary Tree: https://leetcode.com/problems/balanced-binary-tree/
 */
public final class _110_BalancedBinaryTree {
    public static boolean isBalanced(TreeNode root) {
        return checkDeep(root) != -1;
    }

    private static int checkDeep(TreeNode node) {
        if (node == null) {
            return 0;
        }

        int l = checkDeep(node.left);
        if (l == -1) {
            return -1;
        }

        int r = checkDeep(node.right);
        if (r == -1) {
            return -1;
        }

        if (Math.abs(l - r) > 1) {
            return -1;
        }

        return Math.max(l, r) + 1;
    }
}