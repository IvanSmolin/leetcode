package ivan.smolin.problems;

import ivan.smolin.core.TreeUtils.TreeNode;

/**
 * LeetCode Problem 111. Minimum Depth of Binary Tree: https://leetcode.com/problems/minimum-depth-of-binary-tree/
 */
public final class _111_MinimumDepthOfBinaryTree {
    public static int minDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }

        if (root.right == null) {
            return minDepth(root.left) + 1;
        }

        if (root.left == null) {
            return minDepth(root.right) + 1;
        }

        return Math.min(minDepth(root.left), minDepth(root.right)) + 1;
    }
}