package ivan.smolin.problems;

import ivan.smolin.core.TreeUtils.TreeNode;

/**
 * LeetCode Problem 112. Path Sum: https://leetcode.com/problems/path-sum/
 */
public final class _112_PathSum {
    public static boolean hasPathSum(TreeNode root, int targetSum) {
        if (root == null) {
            return false;
        }

        if (root.left == null && root.right == null && root.val == targetSum) {
            return true;
        }

        targetSum -= root.val;

        return hasPathSum(root.left, targetSum) || hasPathSum(root.right, targetSum);
    }
}