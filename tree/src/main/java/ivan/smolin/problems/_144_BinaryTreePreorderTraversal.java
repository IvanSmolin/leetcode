package ivan.smolin.problems;

import ivan.smolin.core.TreeUtils.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * LeetCode Problem 144. Binary Tree Preorder Traversal: https://leetcode.com/problems/binary-tree-preorder-traversal/
 */
public final class _144_BinaryTreePreorderTraversal {
    public static List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<>();

        if (root != null) {
            result.add(root.val);
            result.addAll(preorderTraversal(root.left));
            result.addAll(preorderTraversal(root.right));
        }

        return result;
    }
}