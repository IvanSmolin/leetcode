package ivan.smolin.problems;

import ivan.smolin.core.TreeUtils.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * LeetCode Problem 145. Binary Tree Postorder Traversal:
 * https://leetcode.com/problems/binary-tree-postorder-traversal/
 */
public final class _145_BinaryTreePostorderTraversal {
    public static List<Integer> postorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<>();

        if (root != null) {
            result.addAll(postorderTraversal(root.left));
            result.addAll(postorderTraversal(root.right));
            result.add(root.val);
        }

        return result;
    }
}