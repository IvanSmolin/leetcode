package ivan.smolin.problems;

import ivan.smolin.core.TreeUtils.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * LeetCode Problem 94. Binary Tree Inorder Traversal: https://leetcode.com/problems/binary-tree-inorder-traversal/
 */
public final class _94_BinaryTreeInorderTraversal {
    public static List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<>();

        if (root != null) {
            result.addAll(inorderTraversal(root.left));
            result.add(root.val);
            result.addAll(inorderTraversal(root.right));
        }

        return result;
    }
}