package ivan.smolin.problems;

import ivan.smolin.core.TreeUtils.TreeNode;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._100_SameTree.isSameTree;

/**
 * LeetCode Problem 100. Same Tree: https://leetcode.com/problems/same-tree/
 */
public final class _100_SameTreeTest {
    @Test
    void test1() {
        var tree1 = new TreeNode(1, new TreeNode(2), new TreeNode(3));
        var tree2 = new TreeNode(1, new TreeNode(2), new TreeNode(3));

        var answer = isSameTree(tree1, tree2);

        Assertions.assertTrue(answer);
    }

    @Test
    void test2() {
        var tree1 = new TreeNode(1, new TreeNode(2), new TreeNode(3));
        var tree2 = new TreeNode(1, new TreeNode(2), null);

        var answer = isSameTree(tree1, tree2);

        Assertions.assertFalse(answer);
    }
}