package ivan.smolin.problems;

import ivan.smolin.core.TreeUtils.TreeNode;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._101_SymmetricTree.isSymmetric;

/**
 * LeetCode Problem 101. Symmetric Tree: https://leetcode.com/problems/symmetric-tree/
 */
public final class _101_SymmetricTreeTest {
    @Test
    void test1() {
        var tree = new TreeNode(1);
        tree.left = new TreeNode(2);
        tree.right = new TreeNode(2);
        tree.left.left = new TreeNode(3);
        tree.left.right = new TreeNode(4);
        tree.right.left = new TreeNode(3);
        tree.right.right = new TreeNode(4);

        var answer = isSymmetric(tree);

        Assertions.assertFalse(answer);
    }

    @Test
    void test2() {
        var tree = new TreeNode(1);
        tree.left = new TreeNode(2);
        tree.right = new TreeNode(2);
        tree.left.left = new TreeNode(3);
        tree.left.right = new TreeNode(4);
        tree.right.left = new TreeNode(3);

        var answer = isSymmetric(tree);

        Assertions.assertFalse(answer);
    }

    @Test
    void test3() {
        var tree = new TreeNode(1);
        tree.left = new TreeNode(2);
        tree.right = new TreeNode(2);
        tree.left.left = new TreeNode(3);
        tree.left.right = new TreeNode(4);
        tree.right.left = new TreeNode(3);

        var answer = isSymmetric(tree);

        Assertions.assertFalse(answer);
    }

    @Test
    void test4() {
        var tree = new TreeNode(1);
        tree.left = new TreeNode(2);
        tree.right = new TreeNode(2);
        tree.left.left = new TreeNode(3);
        tree.left.right = new TreeNode(4);
        tree.right.left = new TreeNode(3);

        var answer = isSymmetric(tree);

        Assertions.assertFalse(answer);
    }

    @Test
    void test5() {
        var tree = new TreeNode(1);
        tree.left = new TreeNode(2);
        tree.right = new TreeNode(2);
        tree.left.left = new TreeNode(3);
        tree.left.right = new TreeNode(4);
        tree.right.left = new TreeNode(4);
        tree.right.right = new TreeNode(3);

        var answer = isSymmetric(tree);

        Assertions.assertTrue(answer);
    }

    @Test
    void test6() {
        var tree = new TreeNode(2);
        tree.left = new TreeNode(3);
        tree.right = new TreeNode(3);
        tree.left.left = new TreeNode(4);
        tree.left.right = new TreeNode(5);
        tree.right.left = null;
        tree.right.right = new TreeNode(4);

        var answer = isSymmetric(tree);

        Assertions.assertFalse(answer);
    }
}