package ivan.smolin.problems;

import ivan.smolin.core.TreeUtils.TreeNode;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._104_MaximumDepthOfBinaryTree.maxDepth;

/**
 * LeetCode Problem 104. Maximum Depth of Binary Tree: https://leetcode.com/problems/maximum-depth-of-binary-tree/
 */
public final class _104_MaximumDepthOfBinaryTreeTest {
    @Test
    void test1() {
        var tree = new TreeNode(3);
        tree.left = new TreeNode(9);
        tree.right = new TreeNode(20);
        tree.right.left = new TreeNode(15);
        tree.right.right = new TreeNode(7);

        var answer = maxDepth(tree);

        Assertions.assertEquals(3, answer);
    }
}