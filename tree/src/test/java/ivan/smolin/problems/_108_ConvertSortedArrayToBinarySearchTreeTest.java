package ivan.smolin.problems;

import ivan.smolin.core.AssertionUtils;
import ivan.smolin.core.TreeUtils;
import ivan.smolin.core.TreeUtils.TreeNode;
import org.junit.jupiter.api.Test;

import static ivan.smolin.problems._108_ConvertSortedArrayToBinarySearchTree.sortedArrayToBst;

/**
 * LeetCode Problem 108. Convert Sorted Array to Binary Search Tree:
 * https://leetcode.com/problems/convert-sorted-array-to-binary-search-tree/
 */
public final class _108_ConvertSortedArrayToBinarySearchTreeTest {
    @Test
    void test1() {
        TreeNode node15 = new TreeNode(15);
        TreeNode node6 = new TreeNode(6);
        TreeNode node3 = new TreeNode(3);
        TreeNode node5 = new TreeNode(5, node3, node6);
        TreeNode node10 = new TreeNode(10);
        TreeNode node12 = new TreeNode(12, node10, node15);
        TreeNode node8 = new TreeNode(8, node5, node12);
        TreeUtils.printTree(node8);

        var answer = sortedArrayToBst(new int[]{3, 5, 6, 8, 10, 12, 15});

        System.out.println();
        TreeUtils.printTree(answer);

        AssertionUtils.compareTrees(node8, answer);
    }
}