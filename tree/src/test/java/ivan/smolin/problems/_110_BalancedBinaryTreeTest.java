package ivan.smolin.problems;

import ivan.smolin.core.TreeUtils.TreeNode;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.core.TreeUtils.buildTreeFromArray;
import static ivan.smolin.core.TreeUtils.printTree;
import static ivan.smolin.problems._110_BalancedBinaryTree.isBalanced;

/**
 * LeetCode Problem 110. Balanced Binary Tree: https://leetcode.com/problems/balanced-binary-tree/
 */
public final class _110_BalancedBinaryTreeTest {
    @Test
    void test1() {
        TreeNode tree = buildTreeFromArray(new Integer[]{3, 9, 20, null, null, 15, 7});
        printTree(tree);

        var answer = isBalanced(tree);

        Assertions.assertTrue(answer);
    }

    @Test
    void test2() {
        var answer = isBalanced(new TreeNode());

        Assertions.assertTrue(answer);
    }

    @Test
    void test3() {
        TreeNode tree = buildTreeFromArray(new Integer[]{1, 2, 2, 3, 3, null, null, 4, 4});
        printTree(tree);

        var answer = isBalanced(tree);

        Assertions.assertFalse(answer);
    }

    @Test
    void test4() {
        TreeNode tree = buildTreeFromArray(new Integer[]{1, 2, 2, 3, null, null, 3, 4, null, null, 4});
        printTree(tree);

        var answer = isBalanced(tree);

        Assertions.assertFalse(answer);
    }
}