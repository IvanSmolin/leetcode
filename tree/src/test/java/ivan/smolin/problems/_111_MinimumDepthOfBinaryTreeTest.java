package ivan.smolin.problems;

import ivan.smolin.core.TreeUtils.TreeNode;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.core.TreeUtils.buildTreeFromArray;
import static ivan.smolin.core.TreeUtils.printTree;
import static ivan.smolin.problems._111_MinimumDepthOfBinaryTree.minDepth;

/**
 * LeetCode Problem 111. Minimum Depth of Binary Tree: https://leetcode.com/problems/minimum-depth-of-binary-tree/
 */
public final class _111_MinimumDepthOfBinaryTreeTest {
    @Test
    void test1() {
        var tree = buildTreeFromArray(new Integer[]{3, 9, 20, null, null, 15, 7});
        printTree(tree);

        var answer = minDepth(tree);

        Assertions.assertEquals(2, answer);
    }

    @Test
    void test2() {
        var answer = minDepth(new TreeNode());

        Assertions.assertEquals(1, answer);
    }

    @Test
    void test3() {
        var tree = buildTreeFromArray(new Integer[]{1, 2, 2, 3, 3, null, null, 4, 4});
        printTree(tree);

        var answer = minDepth(tree);

        Assertions.assertEquals(2, answer);
    }

    @Test
    void test4() {
        var tree = buildTreeFromArray(new Integer[]{1, 2, 2, 3, null, null, 3, 4, null, null, 4});
        printTree(tree);

        var answer = minDepth(tree);

        Assertions.assertEquals(4, answer);
    }

    @Test
    void test5() {
        var tree = buildTreeFromArray(new Integer[]{3, 9, 20, null, null, 15, 7});
        printTree(tree);

        var answer = minDepth(tree);

        Assertions.assertEquals(2, answer);
    }

    @Test
    void test6() {
        var tree = buildTreeFromArray(new Integer[]{2, null, 3, null, 4, null, 5, null, 6});
        printTree(tree);

        var answer = minDepth(tree);

        Assertions.assertEquals(5, answer);
    }

    @Test
    void test7() {
        var answer = minDepth(null);

        Assertions.assertEquals(0, answer);
    }
}