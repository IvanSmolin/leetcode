package ivan.smolin.problems;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static ivan.smolin.core.TreeUtils.buildTreeFromArray;
import static ivan.smolin.core.TreeUtils.printTree;
import static ivan.smolin.problems._112_PathSum.hasPathSum;

/**
 * LeetCode Problem 112. Path Sum: https://leetcode.com/problems/path-sum/
 */
public final class _112_PathSumTest {
    @Test
    void test1() {
        var tree = buildTreeFromArray(new Integer[]{5, 4, 8, 11, null, 13, 4, 7, 2, null, null, null, 1});
        printTree(tree);

        var answer = hasPathSum(tree, 22);

        Assertions.assertTrue(answer);
    }

    @Test
    void test2() {
        var tree = buildTreeFromArray(new Integer[]{1, 2, 3});
        printTree(tree);

        var answer = hasPathSum(tree, 5);

        Assertions.assertFalse(answer);
    }

    @Test
    void test3() {
        var answer = hasPathSum(null, 0);

        Assertions.assertFalse(answer);
    }
}