package ivan.smolin.problems;

import ivan.smolin.core.TreeUtils.TreeNode;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static ivan.smolin.core.TreeUtils.buildTreeFromArray;
import static ivan.smolin.core.TreeUtils.printTree;
import static ivan.smolin.problems._144_BinaryTreePreorderTraversal.preorderTraversal;

/**
 * LeetCode Problem 144. Binary Tree Preorder Traversal: https://leetcode.com/problems/binary-tree-preorder-traversal/
 */
public final class _144_BinaryTreePreorderTraversalTest {
    @Test
    void test1() {
        var tree = buildTreeFromArray(new Integer[]{1, null, 2, 3});
        printTree(tree);

        var answer = preorderTraversal(tree);

        Assertions.assertEquals(List.of(1, 2, 3), answer);
    }

    @Test
    void test2() {
        var answer = preorderTraversal(null);

        Assertions.assertEquals(List.of(), answer);
    }

    @Test
    void test3() {
        var answer = preorderTraversal(new TreeNode(1));

        Assertions.assertEquals(List.of(1), answer);
    }
}