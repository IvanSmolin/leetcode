package ivan.smolin.problems;

import ivan.smolin.core.TreeUtils.TreeNode;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static ivan.smolin.core.TreeUtils.buildTreeFromArray;
import static ivan.smolin.core.TreeUtils.printTree;
import static ivan.smolin.problems._145_BinaryTreePostorderTraversal.postorderTraversal;

/**
 * LeetCode Problem 145. Binary Tree Postorder Traversal:
 * https://leetcode.com/problems/binary-tree-postorder-traversal/
 */
public final class _145_BinaryTreePostorderTraversalTest {
    @Test
    void test1() {
        var tree = buildTreeFromArray(new Integer[]{1, null, 2, 3});
        printTree(tree);

        var answer = postorderTraversal(tree);

        Assertions.assertEquals(List.of(3, 2, 1), answer);
    }

    @Test
    void test2() {
        var answer = postorderTraversal(null);

        Assertions.assertEquals(List.of(), answer);
    }

    @Test
    void test3() {
        var answer = postorderTraversal(new TreeNode(1));

        Assertions.assertEquals(List.of(1), answer);
    }

    @Test
    void test4() {
        var tree = buildTreeFromArray(new Integer[]{3, 1, 2});
        printTree(tree);

        var answer = postorderTraversal(tree);

        Assertions.assertEquals(List.of(1, 2, 3), answer);
    }
}