package ivan.smolin.problems;

import ivan.smolin.core.TreeUtils.TreeNode;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static ivan.smolin.problems._94_BinaryTreeInorderTraversal.inorderTraversal;

/**
 * LeetCode Problem 94. Binary Tree Inorder Traversal: https://leetcode.com/problems/binary-tree-inorder-traversal/
 */
public final class _94_BinaryTreeInorderTraversalTest {
    @Test
    void test1() {
        var tree = new TreeNode();
        tree = new TreeNode(1);
        tree.left = new TreeNode(2);
        tree.right = new TreeNode(3);
        tree.left.left = new TreeNode(4);
        tree.left.right = new TreeNode(5);

        var answer = inorderTraversal(tree);

        Assertions.assertEquals(Arrays.asList(4, 2, 5, 1, 3), answer);
    }

    @Test
    void test2() {
        var tree = new TreeNode();
        tree = new TreeNode(1);
        tree.right = new TreeNode(2);
        tree.right.left = new TreeNode(3);

        var answer = inorderTraversal(tree);

        Assertions.assertEquals(Arrays.asList(1, 3, 2), answer);
    }

    @Test
    void test3() {
        var answer = inorderTraversal(null);

        Assertions.assertEquals(List.of(), answer);
    }

    @Test
    void test4() {
        var tree = new TreeNode(1);

        var answer = inorderTraversal(tree);

        Assertions.assertEquals(Arrays.asList(1), answer);
    }
}